@extends('layouts.app') @section('content')



    <div class="panel panel-default" style="text-align: center; margin: auto; width: 90% ">

        <p></p>
        <p></p>
        <div class="panel-body">
            <table class="table table-bordered table-responsive table-striped">
                <tr>
                    <th width="1%">id</th>
                    <th width="1%">Name</th>
                    <th width="1%">Username</th>
                    <th width="1%">E-mail</th>
                    <th width="1%">phone</th>
                    <th width="1%">Action</th>
                </tr>
                <tr>
                    <td colspan="13" class="light-green-background no-padding" title="Create new template">
                        <div class="row centered-child">
                            <div class="col-md-20">
                            </div>
                        </div>
                    </td>
                </tr>
                @foreach($users as $model)
                    <tr>
                        <td>{{$model->id}}</td>
                        <td>{{$model->name}}</td>
                        <td>{{$model->username}}</td>
                        <td>{{$model->email}}</td>
                        <td>{{$model->phone}}</td>
                        <td style="text-align: left">

{{--                            {{Form::open(['class' => 'confirm-delete', 'route' => ['courier.destroy', $model->id], 'method' => 'DELETE'])}}--}}
{{----}}
                            {{ link_to_route('users.show', 'View', [$model->id], ['class' => 'btn btn-success btn-xs']) }}
{{--                            {{Form::button('Удалить', ['class' => 'btn btn-danger btn-xs', 'type' => 'submit'])}} {{Form::close()}}--}}
{{--                            {{Form::close()}}--}}
                        </td>
                    </tr>
                @endforeach

            </table>
            {{$users->links()}}
        </div>
    </div>
@endsection
