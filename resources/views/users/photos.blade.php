@extends('layouts.app') @section('content')

    <div class="panel panel-default" style="text-align: center; margin: auto; width: 90% ">

        <p></p>
        <p></p>
        <div class="panel-body">
            <table class="table table-bordered table-responsive table-striped">
                <tr>
{{--                    <th width="1%">id</th>--}}
                    <th width="1%">Photo</th>
{{--                    <th width="1%">Action</th>--}}
                </tr>
                <tr>
                    <td colspan="13" class="light-green-background no-padding" title="Create new template">
                        <div class="row centered-child">
                            <div class="col-md-20">
                            </div>
                        </div>
                    </td>
                </tr>
                @foreach($photos as $model)
                    <tr>
{{--                        <td>{{$model->id}}</td>--}}
                        <td><img src="{{url(\Illuminate\Support\Facades\Storage::url($model->path . $model->name))}}"  class="scale" width="250px" height="300"></td>
{{--                        <td ><img src="https://fizl.semper.team/storage/user/1/1640094862811972.png" class="scale" width="500px" height="500px"></td>--}}

{{--                        <td style="text-align: left">--}}

                            {{--                            {{Form::open(['class' => 'confirm-delete', 'route' => ['courier.destroy', $model->id], 'method' => 'DELETE'])}}--}}
                            {{----}}
                            {{--                            {{ link_to_route('courier.edit', 'Редактировать', [$model->id], ['class' => 'btn btn-success btn-xs']) }}--}}
                            {{--                            {{Form::button('Удалить', ['class' => 'btn btn-danger btn-xs', 'type' => 'submit'])}} {{Form::close()}}--}}
                            {{--                            {{Form::close()}}--}}
{{--                        </td>--}}
                    </tr>
                @endforeach
            </table>
            {{ link_to_route('approved', 'Approved', [$user->id],['class' => 'btn btn-success btn-xs']) }}
            {{ link_to_route('rejected', 'Rejected', [$user->id],['class' => 'btn btn-danger btn-xs']) }}

        </div>
    </div>
@endsection
