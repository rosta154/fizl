<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('limit_id')->constrained('limits');
            $table->foreignId('expiration_id')->constrained('expirations');
            $table->foreignId('trial_duration_id')->nullable()->constrained('trial_durations');
            $table->foreignId('discount_id')->nullable()->constrained('discounts');
            $table->foreignId('user_id')->constrained('users');
            $table->text('message')->nullable();
            $table->string('type');
            $table->decimal('price')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
