<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');

            $table->integer('toast_new_comment')->default(0);
            $table->integer('toast_new_like')->default(0);
            $table->integer('email')->default(0);
            $table->integer('app_new_comment')->default(0);
            $table->integer('app_new_like')->default(0);
            $table->integer('app_discounts')->default(0);
            $table->integer('app_upcoming')->default(0);
            $table->integer('push')->default(0);
            $table->integer('activity_status')->default(0);
            $table->integer('subscription_offers')->default(0);
            $table->integer('dark_mode')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
