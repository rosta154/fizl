<?php

namespace Database\Seeders;

use App\Models\Lists;
use Illuminate\Database\Seeder;
use DB;
class ListsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lists')->insert([
            ['title' => 'Blocked','type' => Lists::TYPE_DEFAULT],
            ['title' => 'Restricted','type' => Lists::TYPE_DEFAULT],
            ['title' => 'Close Friends','type' => Lists::TYPE_DEFAULT],
            ['title' => 'Bookmarks','type' => Lists::TYPE_DEFAULT],
            ['title' => 'Following','type' => Lists::TYPE_DEFAULT],
        ]);
    }
}
