<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class ExpirationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('expirations')->insert([
            ['title' => '7 days'],
            ['title' => '14 days'],
            ['title' => '21 days'],
            ['title' => '30 days'],
        ]);
    }
}
