<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects')->insert([
            ['title' => 'Registration and Verification'],
            ['title' => 'Profile Access'],
            ['title' => 'Credit Card Payments'],
            ['title' => 'Payouts'],
            ['title' => 'Cancel my payouts request'],
            ['title' => 'Technical Questions'],
            ['title' => 'Reporting Stolen Content'],
            ['title' => 'Law Enforcement/ Legal Matter'],
            ['title' => 'Report Illegal Material or Behavior'],
            ['title' => 'Other'],
        ]);
    }
}
