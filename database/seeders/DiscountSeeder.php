<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class DiscountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('discounts')->insert([
            ['title' => '0%'],
            ['title' => '5%'],
            ['title' => '10%'],
            ['title' => '15%'],
            ['title' => '20%'],
            ['title' => '25%'],
            ['title' => '30%'],
            ['title' => '35%'],
            ['title' => '40%'],
            ['title' => '45%'],
            ['title' => '50%'],
        ]);
    }
}
