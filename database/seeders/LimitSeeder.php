<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class LimitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('limits')->insert([
            ['title' => '5 Subscriptions'],
            ['title' => '10 Subscriptions'],
            ['title' => '15 Subscriptions'],
            ['title' => '20 Subscriptions'],
            ['title' => '25 Subscriptions'],
            ['title' => '30 Subscriptions'],
        ]);
    }
}
