<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class TrialDurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trial_durations')->insert([
            ['title' => '3 days'],
            ['title' => '7 days'],
            ['title' => '14 days'],
            ['title' => '21 days'],
            ['title' => '30 days'],
        ]);
    }
}
