<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class GenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genders')->insert([
            ['title' => 'Male'],
            ['title' => 'Female'],
        ]);
    }
}
