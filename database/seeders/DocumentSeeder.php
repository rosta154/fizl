<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class DocumentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('documents')->insert([
            ['title' => 'Passport'],
            ['title' => 'Car license']
        ]);
    }
}
