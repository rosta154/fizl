<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            SubjectSeeder::class,
            ListsSeeder::class,
            GenderSeeder::class,
            DiscountSeeder::class,
            DurationSeeder::class,
            ExpirationSeeder::class,
            LimitSeeder::class,
            TrialDurationSeeder::class,
            DocumentSeeder::class
        ]);
    }
}
