<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class DurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('durations')->insert([
            ['title' => '1 month'],
            ['title' => '2 month'],
            ['title' => '3 month'],
            ['title' => '4 month'],
            ['title' => '5 month'],
            ['title' => '6 month'],
            ['title' => '7 month'],
            ['title' => '8 month'],
            ['title' => '9 month'],
            ['title' => '10 month'],
            ['title' => '11 month'],
            ['title' => '12 month'],
        ]);
    }
}
