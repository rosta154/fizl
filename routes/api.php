<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\NetworkController;
use App\Http\Controllers\DeviceController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\BookmarkController;
use App\Http\Controllers\CommentAnswerController;
use App\Http\Controllers\BlacklistController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\ContactUsController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\RestrictedController;
use App\Http\Controllers\ListsController;
use App\Http\Controllers\LoginSessionController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\UserDeleteController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\BundleController;
use App\Http\Controllers\PromotionController;
use App\Http\Controllers\StripeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [LoginController::class, 'loginUser']);
Route::post('forgot-password', [UserController::class, 'forgotPassword']);
Route::post('user/check-code', [UserController::class, 'verifyCode']);
Route::post('payouts', [StripeController::class, 'payouts']);

Route::get('successPayment', [StripeController::class, 'success']);
Route::post('check', [StripeController::class, 'check']);
Route::get('cancelPayment', [StripeController::class, 'cancel']);
Route::group(['middleware' => ['auth:api']], function () {

    Route::put('user/update', [UserController::class, 'update']);
    Route::post('user/avatar-update', [UserController::class, 'avatar']);
    Route::post('user/banner-update', [UserController::class, 'banner']);
    Route::get('profile', [UserController::class, 'myProfile']);
    Route::get('profile/{user}', [UserController::class, 'profile']);
    Route::post('user-online', [UserController::class, 'online']);
    Route::post('user-search', [UserController::class, 'search']);
    Route::delete('avatar-delete', [UserController::class, 'deletePhoto']);
    Route::put('user/change-password', [UserController::class, 'changePassword']);
    Route::put('user/two-step-auth', [UserController::class, 'twoStepAuth']);


    Route::post('post', [PostController::class, 'store']);
    Route::post('post-edit/{post}', [PostController::class, 'update']);
    Route::put('poll/{post}', [PostController::class, 'updatePoll']);
    Route::delete('post/{post}', [PostController::class, 'delete']);
    Route::get('post/{post}', [PostController::class, 'show']);
    Route::get('post-counter/{user}', [PostController::class, 'myPosts']);
    Route::get('posts/{type}/{user}', [PostController::class, 'posts']);
    Route::get('option/{option}', [PostController::class, 'optionAnswer']);
    Route::get('comments-post/{post}', [PostController::class, 'comments']);
    Route::post('posts-by-user/{user}', [PostController::class, 'postByUser']);
    Route::post('post-search/{type}/{user}', [PostController::class, 'search']);
    Route::post('post-feed/{user}', [PostController::class, 'feed']);
    Route::post('my-posts/{user}', [PostController::class, 'myPost']);


    Route::post('comment', [CommentController::class, 'store']);
    Route::delete('comment/{comment}', [CommentController::class, 'delete']);
    Route::post('comment/{comment}/reaction', [CommentController::class, 'reaction']);
    Route::post('post/{post}/reaction', [PostController::class, 'reaction']);

    Route::post('network', [NetworkController::class, 'store']);
    Route::delete('network/{user}', [NetworkController::class, 'delete']);
    Route::get('check-network/{user}', [NetworkController::class, 'checkFriend']);
    Route::get('network/{user}', [NetworkController::class, 'index']);
    Route::get('fans/{user}', [NetworkController::class, 'subscriber']);

    Route::post('subscribe-notification', [DeviceController::class, 'subscribe']);
    Route::post('unsubscribe-notification', [DeviceController::class, 'logout']);

    Route::get('my-notifications', [NotificationController::class, 'index']);
    Route::get('my-notifications/{type}', [NotificationController::class, 'notifications']);
    Route::get('notifications', [SettingsController::class, 'myNotification']);
    Route::put('my-notifications', [SettingsController::class, 'enabling']);

    Route::post('post/{post}/bookmark', [BookmarkController::class, 'store']);
    Route::get('myBookmarks', [BookmarkController::class, 'index']);
    Route::get('myBookmarks/{type}', [BookmarkController::class, 'bookmarksByType']);
    Route::post('myBookmarks-filter/{type}', [BookmarkController::class, 'bookmarksByFilter']);

    Route::resource('answer', CommentAnswerController::class);
    Route::get('answers/{comment}', [CommentAnswerController::class, 'index']);
    Route::post('answer/{comment}/reaction', [CommentAnswerController::class, 'reaction']);

    Route::post('blacklist', [BlacklistController::class, 'store']);
    Route::delete('blacklist/{user}', [BlacklistController::class, 'delete']);
    Route::get('check-blacklist/{user}', [BlacklistController::class, 'checkFriend']);
    Route::get('blacklist/{user}', [BlacklistController::class, 'index']);

    Route::get('subjects', [SubjectController::class, 'index']);
    Route::post('contactUs', [ContactUsController::class, 'store']);
    Route::get('news', [NewsController::class, 'index']);

    Route::get('add-to-close-friends/{user}', [NetworkController::class, 'closeFriend']);
    Route::get('remove-from-close-friends/{user}', [NetworkController::class, 'deleteCloseFriend']);
    Route::get('close-friends/{user}', [NetworkController::class, 'closeFriendList']);

    Route::post('check-username', [UserController::class, 'checkUsername']);

    Route::post('restricted', [RestrictedController::class, 'store']);
    Route::delete('restricted/{user}', [RestrictedController::class, 'delete']);
    Route::get('check-restricted/{user}', [RestrictedController::class, 'checkFriend']);
    Route::get('restricted/{user}', [RestrictedController::class, 'index']);

    Route::post('list', [ListsController::class, 'store']);


    Route::get('my-sessions/{session}', [LoginSessionController::class, 'mySessions']);
    Route::post('session', [LoginSessionController::class, 'store']);
    Route::delete('session/{session}', [LoginSessionController::class, 'delete']);
    Route::delete('sessions/{session}', [LoginSessionController::class, 'removeAll']);

    Route::post('check-phone', [UserController::class, 'checkPhone']);
    Route::post('update-phone', [UserController::class, 'updatePhone']);
    Route::post('phone/check-code', [UserController::class, 'verifyCodePhone']);
    Route::post('customList/{lists}', [ListsController::class, 'addtolist']);
    Route::get('customList/{lists}', [ListsController::class, 'lists']);
    Route::get('my-customList', [ListsController::class, 'mylists']);
    Route::post('my-lists', [ListsController::class, 'allLists']);
    Route::put('lists/{lists}', [ListsController::class, 'update']);
    Route::post('remove/customList/{lists}', [ListsController::class, 'removeFromList']);

    Route::post('post/report', [ReportController::class, 'store']);

    Route::post('user-delete/send-code', [UserDeleteController::class, 'sendCode']);
    Route::post('user-delete/check-code', [UserDeleteController::class, 'verifyCode']);

    Route::get('genders', [UserController::class, 'genders']);
    Route::resource('messages', MessageController::class)->only([
        'store'
    ]);
    Route::get('messages/{user}', [MessageController::class, 'index']);
    Route::post('search-messages/{user}', [MessageController::class, 'searchMessage']);
    Route::get('myChats', [MessageController::class, 'chats']);
    Route::get('myChats/sort/{filter}', [MessageController::class, 'MyChats']);
    Route::post('myChats/search', [MessageController::class, 'search']);
    Route::put('message-update/{message}', [MessageController::class, 'update']);
    Route::delete('message-delete/{message}', [MessageController::class, 'destroy']);
    Route::get('mute/{user}', [MessageController::class, 'mute']);
    Route::get('history-media/{user}', [MessageController::class, 'historyMedia']);
    Route::post('message/{message}/reaction', [MessageController::class, 'reaction']);

    Route::post('bundle', [BundleController::class, 'store']);
    Route::get('bundles', [BundleController::class, 'index']);
    Route::delete('bundles/{bundle}', [BundleController::class, 'destroy']);
    Route::get('durations', [BundleController::class, 'durations']);
    Route::get('discounts', [BundleController::class, 'discounts']);

    Route::post('promotions', [PromotionController::class, 'store']);
    Route::get('expiration', [PromotionController::class, 'expiration']);
    Route::get('limits', [PromotionController::class, 'limits']);
    Route::get('trial-durations', [PromotionController::class, 'durations']);
    Route::get('promotions', [PromotionController::class, 'index']);

    Route::post('add-card', [StripeController::class, 'saveCard']);
    Route::post('add-bank', [StripeController::class, 'addBank']);
    Route::post('verify-card', [StripeController::class, 'verifyCard']);
    Route::post('payouts', [StripeController::class, 'payouts']);
    Route::post('product', [StripeController::class, 'product']);

    Route::post('user/verify', [UserController::class, 'verify']);
    Route::get('documents', [UserController::class, 'documents']);

    Route::post('pay', [StripeController::class, 'pay']);
    Route::post('card-token', [StripeController::class, 'cardToken']);
    Route::post('subscription-price', [BundleController::class, 'price']);
    Route::get('my-bundles', [UserController::class, 'bundles']);
    Route::get('my-cards', [UserController::class, 'myCards']);
    Route::post('pay-token', [StripeController::class, 'payToken']);
    Route::get('bundles/{user}', [BundleController::class, 'bundles']);

    Route::post('pay-token-tips/{post}', [PostController::class, 'tips']);

});


Broadcast::routes(['middleware' => ['auth:api']]);
