<?php
require '../vendor/autoload.php';

\Stripe\Stripe::setApiKey('sk_test_51KACWHLoIcQRAFhtAXVE7euhvmvWBd2s7uCU3bAGKn0M34HwUf5PSKp3R2nIg4fO0A0FNJaZeHg9iwb3ZY1RBbou0099UAZRat');

$endpoint_secret = 'whsec_LTSCqFwX8uQMNhrbqQDo8CnXdSPnw2v4';
$payload = @file_get_contents('php://input');
$event = null;
try {
    $event = \Stripe\Event::constructFrom(
        json_decode($payload, true)
    );
} catch (\UnexpectedValueException $e) {
    // Invalid payload
    echo '⚠️  Webhook error while parsing basic request.';
    http_response_code(400);
    exit();
}
if ($endpoint_secret) {
    // Only verify the event if there is an endpoint secret defined
    // Otherwise use the basic decoded event
    $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
    try {
        $event = \Stripe\Webhook::constructEvent(
            $payload, $sig_header, $endpoint_secret
        );
    } catch (\Stripe\Exception\SignatureVerificationException $e) {
        // Invalid signature
        echo '⚠️  Webhook error while validating signature.';
        http_response_code(400);
        exit();
    }
}

// Handle the event
switch ($event->type) {

    case 'checkout.session.completed':
        $session = $event->data->object;
        $subscription = \App\Models\UserSubscription::whereId($session->client_reference_id)->first();
        $subscription->update(['status' => \App\Models\Subscription::STATUS_APPROVED]);
        break;
    default:
        // Unexpected event type
        error_log('Received unknown event type');
}

http_response_code(200);

