<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    use HasFactory;

    protected $fillable = ['post_id', 'name'];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
