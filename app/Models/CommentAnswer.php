<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommentAnswer extends Model
{
    use HasFactory;

    protected $fillable = ['parent_id', 'user_id', 'message'];

    use  SoftDeletes;

    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function reactions()
    {
        return $this->morphMany(Reaction::class, 'reactionable');
    }


    public function comment()
    {
        return $this->belongsTo(Comment::class, 'parent_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'answer_recipients', 'answer_id', 'recipient_id');
    }

    public function notifications()
    {
        return $this->morphMany(Notification::class, 'notificationable');
    }
}
