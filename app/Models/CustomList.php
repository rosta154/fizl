<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomList extends Model
{
    use HasFactory;

    protected $fillable = [
        'list_id',
        'customlistable_type',
        'customlistable_id',
        'type',
        'user_id'
    ];

    const TYPE_POST = 'POST';
    const TYPE_USER = 'USER';
    const TYPE_MEDIA = 'MEDIA';


    public function customlistable()
    {
        return $this->morphTo();
    }
}
