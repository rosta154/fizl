<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['description', 'user_id', 'type', 'duration', 'schedule', 'likes','price','pin'];

    const TYPE_MEDIA = 'MEDIA';
    const TYPE_POLL = 'POLL';
    const TYPE_POST = 'POST';
    const TYPE_VIDEO = 'VIDEO';
    const TYPE_PHOTO = 'PHOTO';

    const TYPE_HOME = 0;
    const TYPE_MY_FEED = 1;
    const TYPE_USER_FEED = 2;
    const PROMOTION = 1;

    const PIN = 1;

    public function photo()
    {
        return $this->morphMany(File::class, 'fileable')->where('type', File::FILE_PHOTO);
    }

    public function video()
    {
        return $this->morphMany(File::class, 'fileable')->where('type', File::FILE_VIDEO);
    }

    public function options()
    {
        return $this->hasMany(Option::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function reactions()
    {
        return $this->morphMany(Reaction::class, 'reactionable');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'post_users', 'post_id', 'user_id');
    }

    public function bookmarks()
    {
        return $this->hasMany(Bookmark::class);
    }

    public function customlist()
    {
        return $this->morphMany(CustomList::class, 'customlistable')->where('type', CustomList::TYPE_POST);
    }

    public function report()
    {
        return $this->morphMany(Report::class, 'reportable');
    }

    public function paymentable()
    {
        return $this->morphOne(Payment::class, 'paymentable')->latest();
    }

}
