<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $fillable = [
        'message',
        'sender_id',
        'recipient_id',
        'check',

    ];

    const SENDING = 0;
    const SENT = 1;
    const READ = 2;

    public function recipient()
    {
        return $this->belongsTo(User::class, 'recipient_id');
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    public function photo()
    {
        return $this->morphOne(File::class, 'fileable')->where('type', File::FILE_PHOTO);
    }

    public function reactions()
    {
        return $this->morphMany(Reaction::class, 'reactionable');
    }
    public function photos()
    {
        return $this->morphMany(File::class, 'fileable')->where('type', File::FILE_PHOTO);
    }

    public function videos()
    {
        return $this->morphMany(File::class, 'fileable')->where('type', File::FILE_VIDEO);
    }


    public function report()
    {
        return $this->morphMany(Report::class, 'reportable');
    }
}
