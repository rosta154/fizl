<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'username',
        'bio',
        'location',
        'website',
        'online',
        'two_step_sms',
        'two_step_face',
        'phone',
        'date_of_birth',
        'gender_id',
        'last_seen',
        'verify',
        'subscription_price',
        'balance',
        'country',
        'code'
    ];
    const VERIFY_PENDING = 1;
    const VERIFY = 2;
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function photo()
    {
        return $this->morphOne(File::class, 'fileable')->where('type', File::FILE_PHOTO);
    }

    public function banner()
    {
        return $this->morphOne(File::class, 'fileable')->where('type', File::FILE_BANNER);
    }

    public function post()
    {
        return $this->hasMany(Post::class);
    }

    public function postAsc()
    {
        return $this->hasMany(Post::class)->orderBy('id', 'asc');
    }

    public function postDesc()
    {
        return $this->hasMany(Post::class)->orderBy('id', 'desc');
    }

    public function postAscLikes()
    {
        return $this->hasMany(Post::class)->orderBy('likes', 'asc');
    }

    public function postDescLikes()
    {
        return $this->hasMany(Post::class)->orderBy('likes', 'desc');
    }

    public function options()
    {
        return $this->belongsToMany(Option::class);
    }

    public function notifications()
    {
        return $this->hasOne(Settings::class, 'user_id');
    }

    public function bookmarks()
    {
        return $this->belongsToMany(Post::class, 'bookmarks', 'user_id', 'post_id')->orderBy('bookmarks.id', 'desc');
    }

    public function bookmarksAll()
    {
        return $this->belongsToMany(Post::class, 'bookmarks', 'user_id', 'post_id');
    }

    public function customlist()
    {
        return $this->morphMany(CustomList::class, 'customlistable')->where('type', CustomList::TYPE_USER);
    }

    public function subscriber()
    {
        return $this->hasMany(Network::class, 'subscriber_id');
    }

    public function restricted()
    {
        return $this->hasMany(Restricted::class, 'initiator_id');
    }

    public function blacklist()
    {
        return $this->hasMany(Blacklist::class, 'initiator_id');
    }

    public function lists()
    {
        return $this->hasMany(Lists::class, 'user_id');
    }

    public function loginSession()
    {
        return $this->hasMany(LoginSession::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function answer_comments()
    {
        return $this->hasMany(CommentAnswer::class);
    }

    public function reactions()
    {
        return $this->hasMany(Reaction::class);
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function report()
    {
        return $this->morphMany(Report::class, 'reportable');
    }

    public function rooms()
    {
        return $this->belongsToMany(Room::class, 'room_users');
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'sender_id');
    }

    public function mute()
    {
        return $this->hasMany(MuteChat::class, 'sender_id');
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    public function promotions()
    {
        return $this->hasMany(Promotion::class);
    }

    public function promotion()
    {
        return $this->hasOne(Promotion::class);
    }

    public function verification()
    {
        return $this->hasMany(UserVerification::class);
    }

    public function userSubscription()
    {
        return $this->hasMany(UserSubscription::class);
    }

    public function customer()
    {
        return $this->hasOne(UserCard::class);
    }

    public function paymentable()
    {
        return $this->morphOne(Payment::class, 'paymentable')->latest();
    }

    public function bundles()
    {
        return $this->hasMany(Bundle::class);
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

}
