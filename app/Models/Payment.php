<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use function Symfony\Component\Translation\t;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = [
        'paymentable_id',
        'paymentable_type',
        'product_id',
        'price_id',
    ];

    public function paymentable()
    {
        return $this->morphTo();
    }

    public function history()
    {
        return $this->hasMany(History::class);
    }

}
