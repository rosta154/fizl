<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Network extends Model
{
    use HasFactory;

    protected $fillable = ['subscriber_id', 'subscription_id','close_friend'];

    const CLOSE_FRIEND = 1;
    const  FRIEND = 0;

    public function notifications()
    {
        return $this->morphMany(Notification::class, 'notificationable');
    }
}
