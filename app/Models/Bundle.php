<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bundle extends Model
{
    use HasFactory;

    protected $fillable = ['price', 'discount_id', 'duration_id', 'user_id', 'status'];

    const STATUS_ACTIVE = 0;
    const STATUS_INACTIVE = 1;

    public function discount()
    {
        return $this->belongsTo(Discount::class);
    }

    public function duration()
    {
        return $this->belongsTo(Duration::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function paymentable()
    {
        return $this->morphOne(Payment::class, 'paymentable');
    }
}
