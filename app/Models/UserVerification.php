<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserVerification extends Model
{
    use HasFactory;

    protected $fillable = ['document_id','user_id'];

    public function photo()
    {
        return $this->morphOne(File::class, 'fileable')->where('type', File::FILE_DOCUMENT);
    }

}
