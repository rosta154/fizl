<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    use HasFactory;


    protected $fillable = [
        'user_id',
        'toast_new_comment',
        'toast_new_like',
        'app_new_comment',
        'app_new_like',
        'email',
        'push',
        'app_discounts',
        'app_upcoming',
        'activity_status',
        'subscription_offers',
        'dark_mode'
    ];

    const YES = 1;
    const NO = 0;
}
