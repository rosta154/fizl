<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lists extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'user_id', 'type'];

    const TYPE_DEFAULT = 0;
    const TYPE_CUSTOM = 1;
    const BLOCKED = 1;
    const RESTRICTED = 2;
    const CLOSE_FRIENDS = 3;
    const BOOKMARKS = 4;
    const FOLLOWING = 5;

    public function posts()
    {
        return $this->hasMany(CustomList::class, 'list_id')->whereType(CustomList::TYPE_POST);
    }

    public function users()
    {
        return $this->hasMany(CustomList::class, 'list_id')->whereType(CustomList::TYPE_USER);
    }

    public function media()
    {
        return $this->hasMany(CustomList::class, 'list_id')->whereType(CustomList::TYPE_MEDIA);
    }
}
