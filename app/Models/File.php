<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;

    const FILE_PHOTO = 'photo';
    const FILE_DOCUMENT = 'document';
    const FILE_VIDEO = 'video';
    const FILE_BANNER = 'banner';

    protected $fillable = [
        'fileable_id',
        'fileable_type',
        'name',
        'path',
        'type',
    ];


    public function fileable()
    {
        return $this->morphTo();
    }

    public function customlist()
    {
        return $this->morphMany(CustomList::class, 'customlistable')->where('type', CustomList::TYPE_MEDIA);
    }


}
