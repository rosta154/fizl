<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    use HasFactory;

    const TYPE_DISCOUNT = 'DISCOUNT';
    const TYPE_FREE_TRIAL = 'FREE_TRIAL';

    protected $fillable = ['limit_id', 'trial_duration_id', 'discount_id', 'expiration_id', 'message', 'type','price'];

    public function limit()
    {
        return $this->belongsTo(Limit::class);
    }

    public function trialDuration()
    {
        return $this->belongsTo(TrialDuration::class, 'trial_duration_id');
    }

    public function discount()
    {
        return $this->belongsTo(Discount::class);
    }

    public function expiration()
    {
        return $this->belongsTo(Expiration::class);
    }

    public function paymentable()
    {
        return $this->morphOne(Payment::class, 'paymentable');
    }

}
