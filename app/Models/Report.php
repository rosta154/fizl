<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasFactory;

    protected $fillable = [
        'reportable_id',
        'reportable_type',
        'user_id',
        'reason',
        'type'
    ];

    const TYPE_USER = 'USER';
    const TYPE_POST = 'POST';
    const TYPE_MESSAGE = 'MESSAGE';

    public function reportable()
    {
        return $this->morphTo();
    }
}
