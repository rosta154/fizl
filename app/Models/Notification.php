<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'user_id',
        'notificationable_type',
        'notificationable_id',
        'type',
        'sender_id'
    ];

    const LIKE_POST = 'LIKE_POST';
    const LIKE_COMMENT = 'LIKE_COMMENT';
    const LIKE_ANSWER = 'LIKE_ANSWER_COMMENT';
    const COMMENT = 'COMMENT';
    const SUBSCRIBE = 'SUBSCRIBE';
    const ANSWER_COMMENT = 'ANSWER_COMMENT';

    public function notificationable()
    {
        return $this->morphTo();
    }
}
