<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reaction extends Model
{
    use HasFactory, SoftDeletes;

    CONST REACTION_DISLIKE = 0;
    const REACTION_LIKE = 1;

    protected $fillable = [
        'user_id',
        'reaction',
        'reactionable_type',
        'reactionable_id'
    ];


    public function reactionable()
    {
        return $this->morphTo();
    }

    public function notifications()
    {
        return $this->morphMany(Notification::class, 'notificationable');
    }
}
