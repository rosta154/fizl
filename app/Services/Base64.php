<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 24.05.2019
 * Time: 15:08
 */

namespace App\Services;


use Storage;

class Base64
{
    /**
     * @param $base64
     * @param $path
     * @return array|false
     */
    public function save($base64, $path)
    {
        $file_data = $base64;
        $file_name = time().rand(000000,999999) . '.' . $this->parseExtension($base64); //generating unique file name;
        @list($type, $file_data) = explode(';', $file_data);
        @list(, $file_data) = explode(',', $file_data);
        if ($file_data != "") {
            return Storage::put($path . '/' . $file_name, base64_decode($file_data)) ? [
                'path' => $path,
                'name' => $file_name
                ] : false;
        } else {
            return false;
        }
    }

    public function parseExtension($base64)
    {
        $format = stristr($base64, ';', true);
        return explode("/", $format)['1'];
    }
}
