<?php

namespace App\Http\Controllers;

use App\Http\Requests\Notification\NotificationRequest;
use App\Http\Resources\Notification\NotificationResource;
use App\Models\Notification;
use App\Models\UserNotification;
use Illuminate\Http\Request;
use App\Http\Resources\Collection;

class NotificationController extends ApiController
{
    /**
     * all notofications
     *
     * @return mixed
     */
    public function index()
    {
        $user = auth()->user();
        $notifications = Notification::orderBy('id', 'desc')->where('user_id', $user->id)->paginate(10);
        return $this->respondWithPagination($notifications, new Collection(NotificationResource::collection($notifications)));
    }

    /**
     * notifications by type
     *
     * @param $type
     * @return mixed
     */
    public function notifications($type)
    {
        $user = auth()->user();
        switch ($type) {

            case 1:
                $notifications = Notification::orderBy('id', 'desc')->where('user_id', $user->id)->paginate(10);
                return $this->respondWithPagination($notifications, new Collection(NotificationResource::collection($notifications)));
            case 3:
                $notifications = Notification::orderBy('id', 'desc')->whereIn('type', [Notification::LIKE_POST, Notification::LIKE_COMMENT,Notification::LIKE_ANSWER])->where('user_id', $user->id)->paginate(10);
                return $this->respondWithPagination($notifications, new Collection(NotificationResource::collection($notifications)));
                break;
            case 2:
                $notifications = Notification::orderBy('id', 'desc')->whereIn('type', [Notification::COMMENT, Notification::ANSWER_COMMENT])->where('user_id', $user->id)->paginate(10);
                return $this->respondWithPagination($notifications, new Collection(NotificationResource::collection($notifications)));
                break;
            case 4:
                $notifications = Notification::orderBy('id', 'desc')->where('type', Notification::SUBSCRIBE)->where('user_id', $user->id)->paginate(10);
                return $this->respondWithPagination($notifications, new Collection(NotificationResource::collection($notifications)));
                break;
        }
    }
}
