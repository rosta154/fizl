<?php

namespace App\Http\Controllers;

use App\Events\MessageSentEvent;
use App\Events\MessageUpdateEvent;
use App\Http\Requests\Chat\ChatRequest;
use App\Http\Requests\Chat\MessageDeleteRequest;
use App\Http\Requests\Chat\MessageUpdateRequest;
use App\Http\Requests\Chat\SearchRequest;
use App\Http\Resources\Chat\MediaResource;
use App\Http\Resources\Chat\SearchResource;
use App\Http\Resources\Post\VideoPhotoResource;
use App\Http\Traits\ChatTrait;
use App\Models\MuteChat;
use App\Models\Network;
use App\Models\Reaction;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\RoomUser;
use App\Models\Room;
use App\Models\Message;
use App\Http\Resources\Collection;
use App\Http\Resources\Chat\ChatResource;
use DB;
use Storage;
use App\Models\File;
use App\Facades\Base64;
use App\Http\Resources\Chat\LastMessageResource;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\File as Files;

class MessageController extends ApiController
{
    use ChatTrait;
    /**
     * chat with User
     *
     * @param User $user
     * @return mixed
     */

    const paginate = 30;

    public function index(User $user)
    {
        $currentUser = auth()->user();
        //комнаты текущего юзера
        $roomsCurrentUser = RoomUser::where('user_id', $currentUser->id)->pluck('room_id');
        // ищем есть ли в его комнате этот юзер
        $checkUserRoom = RoomUser::whereIn('room_id', $roomsCurrentUser)->where('user_id', $user->id)->first();
        //если нет - то создаем комнату
        if (!$checkUserRoom) {
            $room = Room::create([
                'title' => $currentUser->id . $user->id,
            ]);
            $room->users()->attach([$currentUser->id, $user->id]);
            $room_id = $room->id;
        } else {
            $room_id = $checkUserRoom->room_id;
        }
        $muted = MuteChat::where('sender_id', $currentUser->id)->where('recipient_id', $user->id)->exists();
        $chat = Message::orderBy('id', 'desc')->where('sender_id', $user->id)->where('recipient_id', $currentUser->id)
            ->orWhere('sender_id', $currentUser->id)->where('recipient_id', $user->id)
            ->paginate(self::paginate);
        $messages = Message::orderBy('id', 'desc')->where('sender_id', $user->id)->where('recipient_id', $currentUser->id)->where('check', Message::SENT)
            ->get();
        foreach ($messages as $message) {
            $message->update(['check' => Message::READ]);
            broadcast(new MessageUpdateEvent($message, $user, $room_id))->toOthers();

        }

        return $this->respondWithPaginationChat($chat, new Collection(ChatResource::collection($chat)), $room_id, $muted);

    }

    public function searchMessage(User $user, Request $request)
    {
        $currentUser = auth()->user();
        //ищем сколько всего смс в чате
        $chatCount = Message::orderBy('id', 'desc')->where('sender_id', $user->id)->where('recipient_id', $currentUser->id)
            ->orWhere('sender_id', $currentUser->id)->where('recipient_id', $user->id)
            ->count();
        //ищем все смс в чате по ключевым словам
        $messages = Message::where('message', "LIKE", "$request->search%")->where('sender_id', $currentUser->id)->where('recipient_id', $user->id)
            ->orWhere('message', "LIKE", "$request->search%")->where('recipient_id', $currentUser->id)->where('sender_id', $user->id)
            ->get();

        $result = [];
        foreach ($messages as $message) {
            $indexMessage = Message::where('sender_id', $user->id)->where('recipient_id', $currentUser->id)->where('id', '>', $message->id)
                ->orWhere('sender_id', $currentUser->id)->where('recipient_id', $user->id)->where('id', '>', $message->id)
                ->count();
//узнаем номер страницы
            $pageNumber = (int)($indexMessage / self::paginate + 1);
            //все страницы
            $pages = (int)($chatCount / self::paginate + 1);
            $pageNumberNext = $pageNumber + 1;
            if ($pageNumber + 1 > $pages) {
                $nextPage = null;
            } else {
                $nextPage = url('api/messages/' . "$user->id?page=" . $pageNumberNext);
            }
            if ($pageNumber - 1 != 0) {
                $pageNumberLast = $pageNumber - 1;
                $last_page = url('api/messages/' . "$user->id?page=" . $pageNumberLast);
            } else {
                $last_page = null;
            }
            $result[] = [
                'message' => $message,
                'current_url' => url('api/messages/' . "$user->id?page=$pageNumber"),
                'next_url' => $nextPage,
                'last_url' => $last_page,
            ];
        }
        $object = (object)$this->paginate($result, 30, null, $user);
        return $this->respondWithPagination($object, new Collection(SearchResource::collection($object)));

    }

    public function paginate($items, $perPage, $page = null, $user)
    {
        $options = ['path' => url('api/search-messages/' . $user->id)];
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public
    function store(ChatRequest $request)
    {
        $user = auth()->user();
        $roomsCurrentUser = RoomUser::where('user_id', $user->id)->pluck('room_id');
        // ищем есть ли в его комнате этот юзер
        $checkRecipientRoom = RoomUser::whereIn('room_id', $roomsCurrentUser)->where('user_id', $request->recipient_id)->first();
        if ($checkRecipientRoom) {
            DB::beginTransaction();
            $message = $user->messages()->create([
                'message' => $request->input('message'),
                'recipient_id' => $request->input('recipient_id'),
                'check' => Message::SENT
            ]);
            if (!is_null($request->photos)) {
                $this->photo($request, $message);
            }
            if (!is_null($request->videos)) {
                $this->video($request, $message);
            }
            $room_id = $checkRecipientRoom->room_id;


//            $mute = MuteChat::where('sender_id', $request->recipient_id)->where('recipient_id', $user->id)->exists();
//            if (!$mute) {
//            $tokens = Device::where('user_id', $request->recipient_id)->get();
//
//            if ($tokens) {
//                $tokens->each(function ($item) use ($message) {
//                    $this->sendNotification(
//                        $item->token,
//                        "Go Jam",
//                        'New message from ' . $message->sender->username,
//                        ['sender_id' => $message->sender_id]
//                    );
//
//                });
//            }
//            }
//             send event to listeners
            broadcast(new MessageSentEvent($message, $user, $room_id))->toOthers();
            DB::commit();
            return $this->respondSuccess(new ChatResource($message), 'created');
        } else {
            return $this->respondWithError('not open chat with user');
        }
    }

    public
    function photo(Request $request, Message $message)
    {
        DB::beginTransaction();
        $path = 'public/chat/photos' . $message->recipient_id . $message->sender_id . '/';
        foreach ($request->all()['photos'] as $item) {
            $filename = time() . rand(000, 999) . '.' . $item->getClientOriginalExtension();
            $file = Storage::putFileAs($path, new Files($item), $filename);
            if ($file) {
                $message->photos()->create([
                    'name' => $filename,
                    'path' => $path,
                    'type' => File::FILE_PHOTO,
                ]);
            } else {
                return $this->respondWithError('File upload error');
            }
        }
        DB::commit();
        return $this->respondSuccess(new ChatResource($message));
    }

    /**
     * my chats
     *
     * @return mixed
     */
    public
    function chats()
    {
        $user = auth()->user();

        $chatsSender = Message::orderBy('created_at', 'desc')->where('sender_id', $user->id)->where('recipient_id', '!=', $user->id)->pluck('recipient_id')->toArray();
        $chatsRecipient = Message::orderBy('created_at', 'desc')->where('sender_id', '!=', $user->id)->where('recipient_id', $user->id)->pluck('sender_id')->toArray();
        $allChats = array_merge($chatsSender, $chatsRecipient);
        $userIds = array_values(array_unique($allChats));
        $chatsWithUsers = User::whereIn('id', $userIds)->paginate(10);


        $chatsWithUsers->setCollection(
            $chatsWithUsers->sortBy(function ($user, $key) use ($userIds) {
                return array_search($user->id, $userIds);
            })
        );

        return $this->respondWithPagination($chatsWithUsers, new Collection(LastMessageResource::collection($chatsWithUsers)));
    }

    public function MyChats($filter)
    {
        $user = auth()->user();
        switch ($filter) {
            case 0:
                return $this->sortByDesc($user);
                break;
            case 1:
                return $this->sentMessage($user);
                break;
            case 2:
               return $this->readMessage($user);
                break;

        }
    }

    public function search(SearchRequest $request)
    {
        $user = auth()->user();
        $userIds = Network::where('subscriber_id', $user->id)->pluck('subscription_id');
        $chatsWithUsers = User::whereIn('id', $userIds)->where('username', "LIKE", "$request->search%")->paginate(20);
        return $this->respondWithPagination($chatsWithUsers, new Collection(LastMessageResource::collection($chatsWithUsers)));
    }

    public function destroy(Message $message, MessageDeleteRequest $request)
    {
        if ($message->check != Message::READ) {
            $message->delete();
            return $this->respondSuccess('null', 'message deleted');
        } else {
            return $this->respondValidationError('Message was read', '422');
        }

    }

    public function update(Message $message, MessageUpdateRequest $request)
    {
        $user = auth()->user();
        $roomsCurrentUser = RoomUser::where('user_id', $user->id)->pluck('room_id');
        // ищем есть ли в его комнате этот юзер
        $checkRecipientRoom = RoomUser::whereIn('room_id', $roomsCurrentUser)->where('user_id', $message->recipient_id)->first();
        if ($checkRecipientRoom) {
            DB::beginTransaction();

            $room_id = $checkRecipientRoom->room_id;
            $message->update($request->only('message'));

//            send event to listeners
            broadcast(new MessageUpdateEvent($message, $user, $room_id))->toOthers();
            DB::commit();
            return $this->respondSuccess(new ChatResource($message), 'updated');
        } else {
            return $this->respondWithError('not open chat with user');
        }

    }

    public function mute(User $user)
    {
        $currentUser = auth()->user();
        $check = $currentUser->mute()->where('recipient_id', $user->id)->exists();
        if (!$check) {
            $currentUser->mute()->create(['recipient_id' => $user->id]);
            return $this->respondSuccess(0, 'muted');
        } else {
            $currentUser->mute()->where('recipient_id', $user->id)->delete();
            return $this->respondSuccess(1, 'sound on');
        }
    }

    public function historyMedia(User $user)
    {
        $currentUser = auth()->user();
        $photos = Message::where('sender_id', $user->id)->where('recipient_id', $currentUser->id)->whereNull('message')
            ->orWhere('sender_id', $currentUser->id)->where('recipient_id', $user->id)->whereNull('message')->pluck('id');
        $media = File::where('fileable_type', 'App\Models\Message')->whereIn('fileable_id', $photos)->paginate(10);
        return $this->respondWithPagination($media, MediaResource::collection($media));
    }

    public function reaction(Message $message)
    {
        $user = auth()->user();
        $roomsCurrentUser = RoomUser::where('user_id', $user->id)->pluck('room_id');
        // ищем есть ли в его комнате этот юзер
        $checkRecipientRoom = RoomUser::whereIn('room_id', $roomsCurrentUser)->where('user_id', $message->sender_id)->first();
        $room_id = $checkRecipientRoom->room_id;
        DB::beginTransaction();
        if ($message->reactions()->where('user_id', auth()->id())->exists()) {
            $message->reactions()->where('user_id', auth()->id())->delete();
            broadcast(new MessageUpdateEvent($message, $user, $room_id))->toOthers();

        } else {

            $like = $message->reactions()->create([
                'user_id' => auth()->id(),
                'reaction' => Reaction::REACTION_LIKE
            ]);


            broadcast(new MessageUpdateEvent($message, $user, $room_id))->toOthers();

        }
        DB::commit();
        return $this->respondSuccess(new ChatResource($message));
    }

    public function video($request, $message)
    {
        $path = 'public/chat/videos/' . $message->recipient_id . $message->sender_id . '/';

        foreach ($request->all()['videos'] as $item) {

            $filename = time() . rand(000, 999) . $item->getClientOriginalName();
            $file = Storage::putFileAs($path, new Files($item), $filename);
            if ($file) {
                $message->videos()->create([
                    'name' => $filename,
                    'path' => $path,
                    'type' => File::FILE_VIDEO,
                ]);
            } else {
                return $this->respondWithError('File upload error');
            }
        }
    }
}
