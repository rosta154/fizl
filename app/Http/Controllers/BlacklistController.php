<?php

namespace App\Http\Controllers;

use App\Http\Requests\Blacklist\BlacklistRequest;
use App\Http\Resources\Collection;
use App\Http\Resources\User\UserResource;
use App\Http\Traits\ListsTrait;
use App\Models\Blacklist;
use App\Models\CustomList;
use App\Models\Lists;
use Illuminate\Http\Request;
use App\Models\User;
use DB;
class BlacklistController extends ApiController
{
    use ListsTrait;
    public function store(BlacklistRequest $request)
    {
        DB::beginTransaction();
        $user = auth()->user();
        $blacklist = new Blacklist($request->all());
        $blacklist->initiator_id = $user->id;
        $blacklist->save();
        $blacklist_user = User::whereId($request->user_id)->first();

        $this->lists($blacklist_user,CustomList::TYPE_USER,$user,Lists::BLOCKED);

        DB::commit();
        return $this->respondCreated('User added as a blacklist', new UserResource(User::find($request->user_id)));
    }

    /**
     * delete a blacklist
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(User $user)
    {

        $network = Blacklist::where('initiator_id', auth()->user()->id)
            ->where('user_id', $user->id)->first();
        $network->delete();
        CustomList::where('list_id', Lists::BLOCKED)
            ->where('type', CustomList::TYPE_USER)
            ->where('user_id', auth()->user()->id)
            ->where('customlistable_id', $user->id)
            ->delete();
        return $this->respondSuccess(  new UserResource(User::find($user->id)),'User has been removed from blacklist');
    }


    /**
     * check a blacklist
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkFriend(User $user)
    {
        $currentUser = auth()->user();
        $check = Blacklist::where('initiator_id', $currentUser->id)->where('user_id', $user->id)->exists();
        if ($check) {
            return $this->respondSuccess(true);
        } else {
            return $this->respondSuccess(false);
        }
    }

    /**
     * blacklist by user
     *
     * @param User $user
     * @return mixed
     */
    public function index(User $user)
    {
        $subscription_ids = Blacklist::where('initiator_id', $user->id)
            ->where('user_id', '!=', $user->id)
            ->pluck('user_id');
        $users = User::whereIn('id', $subscription_ids)->orderBy('username')->paginate(15);
        return $this->respondWithPagination($users, new Collection(UserResource::collection($users)));
    }
}
