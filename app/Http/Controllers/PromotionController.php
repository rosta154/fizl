<?php

namespace App\Http\Controllers;

use App\Http\Requests\Promotion\PromotionRequest;
use App\Http\Resources\Collection;
use App\Http\Resources\Promotion\PromotionResource;
use App\Models\Discount;
use App\Models\Duration;
use App\Models\Expiration;
use App\Models\Limit;
use App\Models\Promotion;
use App\Models\TrialDuration;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PromotionController extends ApiController
{
    const STRIPE_KEY = "sk_test_51KACWHLoIcQRAFhtAXVE7euhvmvWBd2s7uCU3bAGKn0M34HwUf5PSKp3R2nIg4fO0A0FNJaZeHg9iwb3ZY1RBbou0099UAZRat";

    /**
     * list of limits for Promotion
     */
    public function limits()
    {
        return $this->respondSuccess(Limit::all());
    }

    /**
     * list of trial durations for Promotion
     */
    public function durations()
    {
        return $this->respondSuccess(TrialDuration::all());
    }

    /**
     * list of expiration for Promotion
     */
    public function expiration()
    {
        return $this->respondSuccess(Expiration::all());
    }

    /**
     * list of discounts for Promotion
     */
    public function discounts()
    {
        return $this->respondSuccess(Discount::all());
    }

    public function store(PromotionRequest $request)
    {
        if (!is_null($request->discount_id)) {
            $user = auth()->user();
            $stripe = new \Stripe\StripeClient(
                self::STRIPE_KEY
            );

            $percent = ceil(Discount::whereId($request->discount_id)->first()->title) / 100;

            $product = $stripe->products->create([
                'name' => 'Subscription',
            ]);
            $price = $stripe->prices->create(
                [
                    'product' => $product->id,
                    'unit_amount_decimal' => $user->subscription_price - ($user->subscription_price * $percent),
                    'currency' => 'gbp',
                ]
            );
            $data = auth()->user()->promotions()->create
            (array_merge($request->all(),
                [
                    'type' => Promotion::TYPE_DISCOUNT,
                    'price' => $user->subscription_price - ($user->subscription_price * $percent),

                ]));
            $data->paymentable()->create([
                'product_id' => $product->id,
                'price_id' => $price->id
            ]);

        } else {
            $data = auth()->user()->promotions()->create(array_merge($request->all(), ['type' => Promotion::TYPE_FREE_TRIAL]));
        }
        return $this->respondSuccess(new PromotionResource($data));
    }

    public function index()
    {
        return $this->respondSuccess(new Collection(PromotionResource::collection(auth()->user()->promotions)));
    }
}
