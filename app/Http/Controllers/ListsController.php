<?php

namespace App\Http\Controllers;


use App\Http\Requests\CustomList\CustomListRequest;
use App\Http\Requests\Lists\ListsRequest;
use App\Http\Resources\Collection;
use App\Http\Resources\Lists\CustomListsResource;
use App\Http\Resources\Lists\ListsResource;
use App\Http\Resources\Post\PostResource;
use App\Http\Resources\Post\VideoPhotoResource;
use App\Http\Resources\User\UserResource;
use App\Models\Blacklist;
use App\Models\CustomList;
use App\Models\File;
use App\Models\Lists;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class ListsController extends ApiController
{
    public function store(ListsRequest $request)
    {
        $list = auth()->user()->lists()->create($request->validated());
        return $this->respondSuccess($list);
    }

    public function update(ListsRequest $request, Lists $lists)
    {
        $lists->update($request->validated());
        return $this->respondSuccess($lists);

    }

    public function addtolist(Lists $lists, CustomListRequest $request)
    {

        $post = Post::whereId($request->post_id)->first();
        $user = User::whereId($request->user_id)->first();
        $media = File::whereId($request->file_id)->first();
        if ($lists->type == Lists::TYPE_DEFAULT) {
            return $this->defaultLists($user, $lists);
        } else {
            if ($post) {
                return $this->post($post, $lists);
            }
            if ($user) {
                return $this->user($user, $lists);
            }
            if ($media) {
                return $this->media($media, $lists);
            }
        }
    }

    public function post($post, $lists)
    {
        $check = $post->customlist()->where('list_id', $lists->id)->exists();
        if (!$check) {
            $post->customlist()->create([
                'list_id' => $lists->id,
                'type' => CustomList::TYPE_POST
            ]);
            return $this->respondSuccess(new PostResource($post));
        } else {
            return $this->respondValidationError('already added', 422);
        }
    }

    public function user($user, $lists)
    {
        $check = $user->customlist()->where('list_id', $lists->id)->exists();
        if (!$check) {
            $user->customlist()->create([
                'list_id' => $lists->id,
                'type' => CustomList::TYPE_USER
            ]);
            return $this->respondSuccess(new UserResource($user));
        } else {
            return $this->respondValidationError('already added', 422);
        }

    }

    public function media($media, $lists)
    {
        $check = $media->customlist()->where('list_id', $lists->id)->exists();
        if (!$check) {
            $media->customlist()->create([
                'list_id' => $lists->id,
                'type' => CustomList::TYPE_MEDIA
            ]);
            return $this->respondSuccess(new VideoPhotoResource($media));
        } else {
            return $this->respondValidationError('already added', 422);
        }
    }

    public function defaultLists($user, $lists)
    {
        $check = $user->customlist()->where('list_id', $lists->id)->where('user_id', auth()->user()->id)->exists();
        if (!$check) {
            $user->customlist()->create([
                'list_id' => $lists->id,
                'type' => CustomList::TYPE_USER,
                'user_id' => auth()->user()->id,
            ]);

            return $this->respondSuccess(new UserResource($user));
        } else {
            return $this->respondValidationError('already added', 422);
        }

    }

    public function lists(Lists $lists)
    {
        $data = CustomList::where('list_id', $lists->id)->whereNull('user_id')
            ->orWhere('list_id', $lists->id)
            ->where('user_id', auth()->user()->id)->paginate(10);
        return $this->respondWithPagination($data, new Collection(CustomListsResource::collection($data)));

    }

    public function mylists()
    {
        $data = auth()->user()->lists()->paginate(10);
        return $this->respondWithPagination($data, new Collection(ListsResource::collection($data)));

    }

    public function allLists(Request $request)
    {
        switch ($request->filter) {
            case 0:
                $data = auth()->user()->lists()->orderBy('id', 'asc')->orWhereNull('user_id')->orderBy('id', 'asc')->paginate(20);
                return $this->respondWithPagination($data, new Collection(ListsResource::collection($data)));
                break;
            case 1:
                $data = auth()->user()->lists()->orderBy('title', 'asc')->orWhereNull('user_id')->orderBy('id', 'asc')->paginate(20);
                return $this->respondWithPagination($data, new Collection(ListsResource::collection($data)));
                break;
            case 2:
                $data = auth()->user()->lists()->orderBy('id', 'desc')->orWhereNull('user_id')->orderBy('id', 'desc')->paginate(20);
                return $this->respondWithPagination($data, new Collection(ListsResource::collection($data)));
                break;
        }
        $data = auth()->user()->lists()->orderBy('id', 'asc')->orWhereNull('user_id')->orderBy('id', 'asc')->paginate(20);
        return $this->respondWithPagination($data, new Collection(ListsResource::collection($data)));
    }

    public function removeFromList(Lists $lists, Request $request)
    {
        $post = Post::whereId($request->post_id)->first();
        $user = User::whereId($request->user_id)->first();
        if ($user) {
            CustomList::where('list_id', $lists->id)->where('type', CustomList::TYPE_USER)->where('customlistable_id', $user->id)->delete();
            return $this->respondSuccess(new UserResource($user));

        }
        if ($post) {
            CustomList::where('list_id', $lists->id)->where('type', CustomList::TYPE_POST)->where('customlistable_id', $post->id)->delete();
            return $this->respondSuccess(new PostResource($post));

        }
    }
}
