<?php

namespace App\Http\Controllers;

use App\Http\Resources\Post\PostPhotoResource;
use App\Http\Resources\Post\PostResource;
use App\Http\Resources\Post\PostVideoResource;
use App\Http\Traits\BookmarkTrait;
use App\Http\Traits\ListsTrait;
use App\Models\CustomList;
use App\Models\Lists;
use App\Models\Post;
use DB;
use App\Http\Resources\Collection;
use App\Models\File;
use Illuminate\Http\Request;

class BookmarkController extends ApiController
{
    use BookmarkTrait, ListsTrait;

    /**
     * my bookmarks
     *
     * @return mixed
     */
    public function index()
    {
        $user = auth()->user();
        $post = $user->bookmarks()->paginate(10);

        return $this->respondWithPagination($post, new Collection(PostResource::collection($post)));
    }

    /**
     * add/delete a bookmark
     *
     * @param Post $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Post $post)
    {
        DB::beginTransaction();
        $user = auth()->user();
        $check = $post->bookmarks()->where('user_id', $user->id);
        if ($check->exists()) {
            $check->delete();
            CustomList::where('list_id', Lists::BOOKMARKS)
                ->where('type', CustomList::TYPE_POST)
                ->where('user_id', auth()->user()->id)
                ->where('customlistable_id', $post->id)
                ->delete();

        } else {
            $post->bookmarks()->create([
                'user_id' => $user->id
            ]);
            $this->lists($post, CustomList::TYPE_POST, $user, Lists::BOOKMARKS);
        }
        DB::commit();
        return $this->respondSuccess(new PostResource($post));
    }

    public function bookmarksByType($type)
    {
        $user = auth()->user();
        $posts = $user->bookmarks()->whereIn('type', [Post::TYPE_PHOTO, Post::TYPE_VIDEO])->pluck('post_id')->toArray();
        switch ($type) {
            case 1:
                $data = $user->bookmarks()->paginate(10);
                return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                break;
            case 2:
                $videos = File::whereType(File::FILE_VIDEO)->where('fileable_type', 'App\Models\Post')->whereIn('fileable_id', $posts)->paginate(10);
                $videos->setCollection(
                    $videos->sortBy(function ($file, $key) use ($posts) {
                        return array_search($file->fileable_id, $posts);
                    })
                );
                return $this->respondWithPagination($videos, new Collection(PostVideoResource::collection($videos)));
                break;

            case 3:
                $photos = File::whereType(File::FILE_PHOTO)->where('fileable_type', 'App\Models\Post')->whereIn('fileable_id', $posts)->paginate(10);
                $photos->setCollection(
                    $photos->sortBy(function ($file, $key) use ($posts) {
                        return array_search($file->fileable_id, $posts);
                    })
                );
                return $this->respondWithPagination($photos, new Collection(PostPhotoResource::collection($photos)));
                break;
            case 5:
                $data = $user->bookmarks()->whereType(Post::TYPE_POST)->paginate(10);
                return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                break;
        }
    }

    public function bookmarksByFilter($type, Request $request)
    {
        $user = auth()->user();
        switch ($type) {
            case 1:
                return $this->allBookmarks($user, $request);
                break;
            case 2:
                return $this->photos($user, $request);
                break;
            case 3:
                return $this->videos($user, $request);
                break;
            case 4:
                return $this->text($user, $request);
                break;

            case 5:
                return $this->poll($user, $request);
                break;
            case 6:
                return $this->allBookmarks($user, $request);
                break;
        }
    }


}
