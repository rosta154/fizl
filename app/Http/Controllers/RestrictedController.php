<?php

namespace App\Http\Controllers;

use App\Http\Requests\Restricted\RestrictedRequest;
use App\Http\Resources\Collection;
use App\Http\Resources\User\UserResource;
use App\Http\Traits\ListsTrait;
use App\Models\Restricted;
use App\Models\User;
use Illuminate\Http\Request;
use DB;
use App\Models\CustomList;
use App\Models\Lists;

class RestrictedController extends ApiController
{
    use ListsTrait;

    public function store(RestrictedRequest $request)
    {
        DB::beginTransaction();
        $user = auth()->user();
        $restricted = new Restricted($request->all());
        $restricted->initiator_id = $user->id;
        $restricted->save();
        $restricted_user = User::whereId($request->user_id)->first();
        $this->lists($restricted_user, CustomList::TYPE_USER, $user, Lists::RESTRICTED);

        DB::commit();
        return $this->respondCreated('User added as a restricted', new UserResource(User::find($request->user_id)));
    }

    /**
     * delete a restricted
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(User $user)
    {

        $network = Restricted::where('initiator_id', auth()->user()->id)
            ->where('user_id', $user->id)->first();
        $network->delete();
        CustomList::where('list_id', Lists::RESTRICTED)
            ->where('type', CustomList::TYPE_USER)
            ->where('user_id', auth()->user()->id)
            ->where('customlistable_id', $user->id)
            ->delete();

        return $this->respondSuccess(new UserResource(User::find($user->id)), 'User has been removed from restricted');
    }


    /**
     * check a restricted
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkFriend(User $user)
    {
        $currentUser = auth()->user();
        $check = Restricted::where('initiator_id', $currentUser->id)->where('user_id', $user->id)->exists();
        if ($check) {
            return $this->respondSuccess(true);
        } else {
            return $this->respondSuccess(false);
        }
    }

    /**
     * restricted by user
     *
     * @param User $user
     * @return mixed
     */
    public function index(User $user)
    {
        $subscription_ids = Restricted::where('initiator_id', $user->id)
            ->where('user_id', '!=', $user->id)
            ->pluck('user_id');
        $users = User::whereIn('id', $subscription_ids)->orderBy('username')->paginate(15);
        return $this->respondWithPagination($users, new Collection(UserResource::collection($users)));
    }
}
