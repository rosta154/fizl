<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\AvatarRequest;
use App\Http\Requests\User\BannerRequest;
use App\Http\Requests\User\ChangePasswordRequest;
use App\Http\Requests\User\CheckCodePhoneRequest;
use App\Http\Requests\User\CheckCodeRequest;
use App\Http\Requests\User\ForgotPasswordRequest;
use App\Http\Requests\User\OnlineRequest;
use App\Http\Requests\User\PhoneRequest;
use App\Http\Requests\User\TwoAuthRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Http\Requests\User\UsernameRequest;
use App\Http\Requests\Verification\VerificationRequest;
use App\Http\Resources\Bundle\DataResource;
use App\Http\Resources\User\UserResource;
use App\Http\Traits\SmsTrait;
use App\Models\Document;
use App\Models\Gender;
use App\Models\User;
use App\Models\UserCard;
use App\Models\UserPhone;
use App\Models\Verification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Storage;
use DB;
use App\Models\File;
use App\Facades\Base64;
use Illuminate\Support\Facades\Mail;
use App\Http\Resources\Collection;
use JWTAuth;
use Illuminate\Http\File as Files;

class UserController extends ApiController
{
    use SmsTrait;

    /**
     * update user profile
     *
     * @param UpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request)
    {
        $user = auth()->user();
        $user->update($request->validated());
        return $this->respondSuccess(new UserResource($user));
    }

    /**
     *  Upload a avatar
     *
     * @param AvatarRequest $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function avatar(AvatarRequest $request)
    {
        DB::beginTransaction();
        $path = 'public/user/' . auth()->id() . '/';
        $avatar = auth()->user()->photo;
        if ($avatar) {
            Storage::delete($avatar->path . $avatar->name);
            auth()->user()->photo()->delete();
        }

        $fileData = Base64::save($request->input('photo'), $path);
        if (is_array($fileData)) {
            auth()->user()->photo()->create([
                'name' => $fileData['name'],
                'path' => $fileData['path'],
                'type' => File::FILE_PHOTO,
            ]);
        } else {
            return $this->respondWithError('File upload error');
        }
        DB::commit();
        return $this->respondSuccess(new UserResource(auth()->user()));
    }

    /**
     * upload banner
     *
     * @param BannerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function banner(BannerRequest $request)
    {
        DB::beginTransaction();
        $path = 'public/user-banner/' . auth()->id() . '/';
        $banner = auth()->user()->banner;
        if ($banner) {
            Storage::delete($banner->path . $banner->name);
            auth()->user()->banner()->delete();
        }

        $fileData = Base64::save($request->input('banner'), $path);
        if (is_array($fileData)) {
            auth()->user()->banner()->create([
                'name' => $fileData['name'],
                'path' => $fileData['path'],
                'type' => File::FILE_BANNER,
            ]);
        } else {
            return $this->respondWithError('File upload error');
        }
        DB::commit();
        return $this->respondSuccess(new UserResource(auth()->user()));
    }

    /**
     * profile a user
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile(User $user)
    {
        return $this->respondSuccess(new UserResource($user));
    }

    /**
     * my profile
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function myProfile()
    {
        return $this->respondSuccess(new UserResource(auth()->user()));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        $currentUser = auth()->user();
        $user = User::where('username', "LIKE", "$request->username%")->where('id', '!=', $currentUser->id)->get()->take(5);
        return $this->respondSuccess(new Collection(UserResource::collection($user)));
    }

    /**
     * update a status
     *
     * @param OnlineRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function online(OnlineRequest $request)
    {
        $user = auth()->user();
        if ($request->online == 1) {
            $user->update(['last_seen' => null]);
        } else {
            $user->update(['last_seen' => Carbon::now()->format('Y-m-d H:i:s')]);
        }
        $user->update($request->validated());
        return $this->respondSuccess(new UserResource($user));
    }

    /**
     * delete a photo
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePhoto()
    {
        DB::beginTransaction();
        $path = 'public/user/' . auth()->id() . '/';
        $files = Storage::allFiles($path);
        if ($files) {
            Storage::delete($files);
            auth()->user()->photo()->delete();
        }
        DB::commit();
        return $this->respondSuccess(new UserResource(auth()->user()));
    }

    public function forgotPassword(ForgotPasswordRequest $request)
    {
        $user = User::where('email', $request->search)->orWhere('username', $request->search)->first();
        if (is_null($user)) {
            return $this->respondNotFound('User not found');
        } else {
            $new_password = rand(00000000, 99999999);
            $user->update(['password' => Hash::make($new_password)]);
            Mail::send('mail.forgot-password', compact('new_password'), function ($message) use ($user) {
                $message->to("$user->email")
                    ->subject('New password');
            });
            return $this->respondSuccess($user);
        }

    }

    /**
     * change a password
     *
     * @param ChangePasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $user = auth()->user();
        $user->update(['password' => Hash::make($request->new_password)]);
        return $this->respondSuccess($user);

    }

    /**on\off a two step auth
     *
     * @param TwoAuthRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function twoStepAuth(TwoAuthRequest $request)
    {
        $user = auth()->user();
        if (is_null($user->phone) && $request->two_step_sms == 1) {
            return $this->respondValidationError('Add your phone number to your profile', 422);
        }
        $user->update($request->validated());
        return $this->respondSuccess(new UserResource($user));
    }

    /**
     * verify code two step auth
     *
     * @param CheckCodeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyCode(CheckCodeRequest $request)
    {
        $requestPhone = $request->phone;
        $requestCode = $request->code;
        $checkVerification = Verification::where('phone', $requestPhone)->exists();
        if (!$checkVerification) {
            return response()->json(['message' => 'Phone not found'], 404);
        }
        $verification = Verification::where('phone', $requestPhone)->first();
        $check = $this->checkCode($requestCode, $verification->code, $requestPhone, $verification->phone, $request);
        return $check;
    }


    /**
     * check username
     *
     * @param UsernameRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkUsername(UsernameRequest $request)
    {
        $check = User::where('username', $request->username)->exists();
        if (!$check) {
            return $this->respondSuccess(true);
        } else {
            return $this->respondSuccess(false);
        }
    }

    /**
     * check phone
     *
     * @param PhoneRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkPhone(PhoneRequest $request)
    {
        $check = User::where('phone', $request->phone)->exists();
        if (!$check) {
            return $this->respondSuccess(true);
        } else {
            return $this->respondSuccess(false);
        }
    }

    /**
     * send code
     *
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePhone(PhoneRequest $request)
    {
        $user = auth()->user();
        $phone = $request->phone;
        if ($phone == null) {
            return response()->json(['message' => 'Phone not found'], 404);
        }
        $code = $this->generateCode();
        UserPhone::updateOrCreate(['phone' => $phone], [
            'code' => $code,
            'user_id' => $user->id,
            'codeCountry' => $request->codeCountry,
            'country' => $request->country
        ]);
        $send = $this->sendSms($phone, $code);
        return $send;
    }

    public function verifyCodePhone(CheckCodePhoneRequest $request)
    {
        $user = auth()->user();
        $requestPhone = $request->phone;
        $requestCode = $request->code;
        $checkVerification = UserPhone::where('phone', $requestPhone)->where('user_id', $user->id)->exists();
        if (!$checkVerification) {
            return response()->json(['message' => 'Phone not found'], 404);
        }
        $verification = UserPhone::where('phone', $requestPhone)->where('user_id', $user->id)->first();
        $check = $this->checkCodePhone($requestCode, $verification->code, $requestPhone, $verification->phone, $user);
        return $check;
    }

    public function genders()
    {
        return $this->respondSuccess(Gender::all());
    }

    public function documents()
    {
        return $this->respondSuccess(Document::all());
    }

    public function verify(VerificationRequest $request)
    {
        $user = auth()->user();
        $verification = $user->verification()->create([
            'document_id' => $request->document_id
        ]);
        $this->photo($request, $user, $verification);
        $user->update(['verify' => User::VERIFY_PENDING]);
        return $this->respondSuccess($verification, 'documents success added');
    }

    public function photo($request, $user, $verification)
    {
        $path = 'public/user-docs/' . $user->id . '/';
        foreach ($request->all()['photos'] as $item) {
            $filename = time() . rand(000, 999) . '.' . $item->getClientOriginalExtension();
            $file = Storage::putFileAs($path, new Files($item), $filename);
            if ($file) {
                $verification->photo()->create([
                    'name' => $filename,
                    'path' => $path,
                    'type' => File::FILE_DOCUMENT,
                ]);
            } else {
                return $this->respondWithError('File upload error');
            }
        }


    }

    public function bundles()
    {
        $user = auth()->user();
        return $this->respondSuccess(new DataResource($user));
    }

    public function myCards()
    {
        $user = auth()->user();
        $data = UserCard::where('user_id',$user->id)->get();
        return $this->respondSuccess($data);
    }
}

