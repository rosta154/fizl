<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use Illuminate\Http\Request;

class SubjectController extends ApiController
{
    public function index()
    {
        return $this->respondSuccess(Subject::all(), 'all subjects');
    }
}
