<?php

namespace App\Http\Controllers;

use App\Http\Requests\Comment\CommentRequest;
use App\Http\Resources\Comment\CommentResource;
use App\Http\Traits\NotificationTrait;
use App\Http\Traits\PushNotificationTrait;
use App\Models\Comment;
use App\Models\Post;
use App\Models\Reaction;
use App\Models\Notification;
use DB;
use App\Models\Device;

class CommentController extends ApiController
{
    use NotificationTrait, PushNotificationTrait;

    /**
     * add a comment
     *
     * @param CommentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CommentRequest $request)
    {
        $post = Post::whereId($request->post_id)->first();
        $comment = $post->comments()->create([
            'user_id' => auth()->user()->id,
            'message' => $request->message,
        ]);
        if ($request->input('users')) {
            $comment->users()->sync($request->input('users'));
        }
        $tokens = Device::where('user_id', $post->user_id)->get();
        if (count($tokens) != 0 && $post->user_id != auth()->user()->id) {
            $this->pushComment($tokens,$comment->commentable_id);
        }
        if (auth()->user()->id != $post->user_id) {
            $this->notification($comment, Notification::COMMENT, $post->user_id);
        }
        return $this->respondCreated('Created', new CommentResource($comment));
    }

    /**
     * like
     *
     * @param Comment $comment
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function reaction(Comment $comment)
    {
        DB::beginTransaction();
        if ($comment->reactions()->where('user_id', auth()->id())->exists()) {
            $like = $comment->reactions()->where('user_id', auth()->id())->first();
            $like->notifications()->delete();
            $comment->reactions()->where('user_id', auth()->id())->delete();
        } else {
            $tokens = Device::where('user_id', $comment->user_id)->get();
            if (count($tokens) != 0 && $comment->user_id != auth()->user()->id) {
                $this->push($tokens,$comment->commentable_id);
            }
            $like = $comment->reactions()->create([
                'user_id' => auth()->id(),
                'reaction' => Reaction::REACTION_LIKE
            ]);
            if (auth()->user()->id != $comment->user_id) {
                $this->notification($like, Notification::LIKE_COMMENT, $comment->user_id);
            }
        }
        DB::commit();
        return $this->respondSuccess(new CommentResource($comment));
    }

    /**
     * delete a comment
     * @param Comment $comment
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Comment $comment)
    {
        $comment->notifications()->delete();
        $comment->delete();
        return $this->respondSuccess(null, 'deleted');
    }
}
