<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Models\Report;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\User;

class ReportController extends ApiController
{
    //

    public function store(Request $request)
    {
        $post = Post::whereId($request->post_id)->first();
        $user = User::whereId($request->user_id)->first();
        $message = Message::whereId($request->message_id)->first();
        if ($post) {
            $data = $post->report()->create([
                'user_id' => auth()->user()->id,
                'reason' => $request->reason,
                'type' => Report::TYPE_POST
            ]);
            return $this->respondSuccess($data);

        }
        if ($user) {
            $data = $user->report()->create([
                'user_id' => auth()->user()->id,
                'reason' => $request->reason,
                'type' => Report::TYPE_USER
            ]);
            return $this->respondSuccess($data);
        }
        if ($message) {
            $data = $message->report()->create([
                'user_id' => auth()->user()->id,
                'reason' => $request->reason,
                'type' => Report::TYPE_MESSAGE
            ]);
            return $this->respondSuccess($data);
        }
    }
}
