<?php

namespace App\Http\Controllers;

use App\Http\Requests\Settings\SettingsRequest;
use App\Models\Settings;
use Illuminate\Http\Request;

class SettingsController extends ApiController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function myNotification()
    {
        $user = auth()->user();
        $settings = Settings::where('user_id', $user->id)->first();
        return $this->respondSuccess($settings);
    }

    /**
     * on/off notification
     *
     * @param SettingsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function enabling(SettingsRequest $request)
    {
        $settings = Settings::where('user_id', auth()->user()->id)->first();
        $settings->update($request->validated());
        return $this->respondSuccess($settings);

    }
}
