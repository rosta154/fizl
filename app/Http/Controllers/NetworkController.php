<?php

namespace App\Http\Controllers;

use App\Http\Requests\Network\NetworkRequest;
use App\Http\Traits\ListsTrait;
use App\Http\Traits\NotificationTrait;
use App\Models\CustomList;
use App\Models\Device;
use App\Models\Lists;
use App\Models\Network;
use App\Models\Notification;
use App\Models\User;
use DB;
use App\Http\Resources\Collection;
use App\Http\Resources\User\UserResource;

class NetworkController extends ApiController
{
    use NotificationTrait, ListsTrait;

    /**
     * add to friend
     *
     * @param NetworkRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(NetworkRequest $request)
    {
        DB::beginTransaction();
        $user = auth()->user();
        $network = new Network($request->all());
        $network->subscriber_id = $user->id;
        $network->save();
        $this->notification($network, Notification::SUBSCRIBE, $network->subscription_id);
        $subscription = User::whereId($request->subscription_id)->first();
        $tokens = Device::where('user_id', $request->subscription_id)->get();
        if (count($tokens) != 0 && $request->subscription_id != auth()->user()->id) {
            $this->subscribe($tokens, $request->subscription_id);
        }
        $this->lists($subscription, CustomList::TYPE_USER, $user, Lists::FOLLOWING);
        DB::commit();
        return $this->respondCreated('User added as a friend', new UserResource(User::find($request->subscription_id)));
    }

    /**
     * delete a friend
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(User $user)
    {

        $network = Network::where('subscriber_id', auth()->user()->id)
            ->where('subscription_id', $user->id)->first();
        if ($network) {
            $network->notifications()->delete();
            $network->delete();
            CustomList::where('list_id', Lists::FOLLOWING)
                ->where('type', CustomList::TYPE_USER)
                ->where('user_id', auth()->user()->id)
                ->where('customlistable_id', $user->id)
                ->delete();

            CustomList::where('list_id', Lists::CLOSE_FRIENDS)
                ->where('type', CustomList::TYPE_USER)
                ->where('user_id', auth()->user()->id)
                ->where('customlistable_id', $user->id)
                ->delete();
        }
        return $this->respondSuccess(new UserResource($user), 'User has been removed from friends');
    }


    /**
     * check a friend
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkFriend(User $user)
    {
        $currentUser = auth()->user();
        $check = Network::where('subscriber_id', $currentUser->id)->where('subscription_id', $user->id)->exists();
        if ($check) {
            return $this->respondSuccess(true);
        } else {
            return $this->respondSuccess(false);
        }
    }

    /**
     * network by user
     *
     * @param User $user
     * @return mixed
     */
    public function index(User $user)
    {
        $subscription_ids = Network::where('subscriber_id', $user->id)
            ->where('subscription_id', '!=', $user->id)
            ->pluck('subscription_id');
        $users = User::whereIn('id', $subscription_ids)->orderBy('username')->paginate(15);
        return $this->respondWithPagination($users, new Collection(UserResource::collection($users)));
    }

    public function subscriber(User $user)
    {
        $subscription_ids = Network::where('subscription_id', $user->id)
            ->where('subscriber_id', '!=', $user->id)
            ->pluck('subscriber_id');
        $users = User::whereIn('id', $subscription_ids)->orderBy('username')->paginate(4);
        return $this->respondWithPagination($users, new Collection(UserResource::collection($users)));
    }

    /**
     * add to close friend
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function closeFriend(User $user)
    {
        $currentUser = auth()->user();
        $network = Network::where('subscriber_id', $currentUser->id)->where('subscription_id', $user->id);

        if ($network->exists()) {
            $subscription = User::whereId($user->id)->first();

            if (Network::where('subscriber_id', $currentUser->id)
                ->where('subscription_id', $user->id)
                ->where('close_friend', Network::FRIEND)->exists()) {
                $this->lists($subscription, CustomList::TYPE_USER, $currentUser, Lists::CLOSE_FRIENDS);
            }
            $network->update(['close_friend' => Network::CLOSE_FRIEND]);

            return $this->respondSuccess(new UserResource($user), 'added to close friends');
        } else {
            return $this->respondValidationError('This user is not in the friends list', 422);
        }
    }

    /**
     * remove from close friends
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCloseFriend(User $user)
    {
        $currentUser = auth()->user();
        $network = Network::where('subscriber_id', $currentUser->id)->where('subscription_id', $user->id);

        if ($network->exists()) {

            $network->update(['close_friend' => Network::FRIEND]);
            CustomList::where('list_id', Lists::CLOSE_FRIENDS)
                ->where('type', CustomList::TYPE_USER)
                ->where('user_id', auth()->user()->id)
                ->where('customlistable_id', $user->id)
                ->delete();
            return $this->respondSuccess(new UserResource($user), 'remove from list');
        } else {
            return $this->respondValidationError('This user is not in the friends list', 422);
        }
    }

    /**
     * close friend list
     *
     * @param User $user
     * @return mixed
     */
    public function closeFriendList(User $user)
    {
        $subscription_ids = Network::where('subscriber_id', $user->id)
            ->where('subscription_id', '!=', $user->id)
            ->where('close_friend', Network::CLOSE_FRIEND)
            ->pluck('subscription_id');
        $users = User::whereIn('id', $subscription_ids)->orderBy('username')->paginate(15);
        return $this->respondWithPagination($users, new Collection(UserResource::collection($users)));
    }
}
