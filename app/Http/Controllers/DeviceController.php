<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Device\DeviceRequest;
use App\Models\Device;
class DeviceController extends ApiController
{
    /**
     * subscribe to notification
     *
     * @param DeviceRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function subscribe(DeviceRequest $request)
    {
        $user = auth()->user();
        $device = Device::firstOrCreate([
            'user_id' => $user->id,
            'token' => $request->token
        ]);
        return $this->respondSuccess($device, 'push notification was successfully subscribed');
    }

    /**
     * unsubscribe to notification
     *
     * @param DeviceRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(DeviceRequest $request)
    {
        Device::where('token', $request->token)->delete();
        return $this->respondSuccess(null, 'token was successfully deleted');
    }
}
