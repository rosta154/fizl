<?php

namespace App\Http\Controllers;

use App\Http\Requests\Post\PollRequest;
use App\Http\Requests\Post\PostRequest;
use App\Http\Requests\Post\PostUpdateRequest;
use App\Http\Requests\Post\ReportRequest;
use App\Http\Resources\Comment\CommentResource;
use App\Http\Resources\Post\PostPhotoResource;
use App\Http\Resources\Post\PostResource;
use App\Http\Resources\Post\PostVideoResource;
use App\Http\Resources\Post\SearchResource;
use App\Http\Resources\Post\VideoPhotoResource;
use App\Http\Traits\NotificationTrait;
use App\Http\Traits\PostTrait;
use App\Http\Traits\PushNotificationTrait;
use App\Models\Device;
use App\Models\History;
use App\Models\Network;
use App\Models\Notification;
use App\Models\Option;
use App\Models\Payment;
use App\Models\Post;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Storage;
use App\Models\File;
use App\Facades\Base64;
use DB;
use Illuminate\Http\File as Files;
use Illuminate\Support\Str;
use App\Http\Resources\Collection;
use App\Models\Reaction;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Contracts\Pagination\LengthAwarePaginator as Paginate;

class PostController extends ApiController
{
    use PushNotificationTrait, NotificationTrait, PostTrait;

    const STRIPE_KEY = "sk_test_51KACWHLoIcQRAFhtAXVE7euhvmvWBd2s7uCU3bAGKn0M34HwUf5PSKp3R2nIg4fO0A0FNJaZeHg9iwb3ZY1RBbou0099UAZRat";

    /**
     * get a post by id
     *
     * @param Post $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Post $post)
    {
        return $this->respondSuccess(new PostResource($post));
    }


    /**
     * create a post (post or poll)
     *
     * @param PostRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PostRequest $request)
    {
        DB::beginTransaction();
        $stripe = new \Stripe\StripeClient(
            self::STRIPE_KEY
        );
        $post = auth()->user()->post()->create([
            'type' => Post::TYPE_POST,
            'description' => $request->description,
            'duration' => $request->duration,
            'schedule' => $request->schedule,
            'price' => $request->price,
        ]);
        $product = $stripe->products->create([
            'name' => "post: $post->id",
        ]);
        $post->paymentable()->create([
            'product_id' => $product->id,
        ]);
        if (!is_null($request->price)) {
            $this->createProduct($post);
        }
        if (!is_null($request->photos)) {
            $this->photo($request, $post);
        }
        if (!is_null($request->videos)) {
            $this->video($request, $post);
        }
        if (!is_null($request->duration)) {
            $this->poll($request, $post);
        }
        if ($request->input('users')) {
            $post->users()->sync($request->input('users'));
        }
        DB::commit();
        return $this->respondSuccess(new PostResource($post));
    }

    /**
     * added a photos to post
     *
     * @param Request $request
     * @param Post $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function addMedia(Request $request, Post $post)
    {

        $path = 'public/post-media/' . $post->id . '/';
        $files = Storage::allFiles('public/post-media/' . $post->id);
        if ($files) {
            Storage::delete($files);
            $post->photo()->delete();
        }
        $post->update(['type' => Post::TYPE_MEDIA]);
        foreach ($request->all()['media'] as $item) {
            $isbase64 = Str::startsWith($item, 'data');
            if ($isbase64) {
                $this->photo($item, $path, $post);
            } else {
                $this->video($item, $path, $post);
            }

        }
        return $this->respondSuccess(new PostResource($post), 'photos has been added');
    }

    /**
     * create a poll
     *
     * @param Request $request
     * @param Post $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function poll(Request $request, Post $post)
    {

        $post->update(['type' => Post::TYPE_POLL]);
        foreach ($request->input('options') as $option) {
            $post->options()->create(['name' => $option]);
        }
        return $this->respondSuccess(new PostResource($post));
    }

    /**
     * save a photo to post
     *
     * @param $item
     * @param $path
     * @param $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function photo($request, $post)
    {
        $path = 'public/post-photo/' . $post->id . '/';
        $files = Storage::allFiles('public/post-photo/' . $post->id);
//        if ($files) {
//            Storage::delete($files);
//            $post->photo()->delete();
//        }

        $post->update(['type' => Post::TYPE_PHOTO]);
        foreach ($request->all()['photos'] as $item) {
            $filename = time() . rand(000, 999) . '.' . $item->getClientOriginalExtension();
            $file = Storage::putFileAs($path, new Files($item), $filename);
            if ($file) {
                $post->photo()->create([
                    'name' => $filename,
                    'path' => $path,
                    'type' => File::FILE_PHOTO,
                ]);
            } else {
                return $this->respondWithError('File upload error');
            }
        }


    }

    /**
     *save a video to post
     *
     * @param $item
     * @param $path
     * @param $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function video($request, $post)
    {
        $path = 'public/post-video/' . $post->id . '/';
        $files = Storage::allFiles('public/post-video/' . $post->id);
        if ($files) {
            Storage::delete($files);
            $post->video()->delete();
        }
        $post->update(['type' => Post::TYPE_VIDEO]);
        foreach ($request->all()['videos'] as $item) {

            $filename = time() . rand(000, 999) . $item->getClientOriginalName();
            $file = Storage::putFileAs($path, new Files($item), $filename);
            if ($file) {
                $post->video()->create([
                    'name' => $filename,
                    'path' => $path,
                    'type' => File::FILE_VIDEO,
                ]);
            } else {
                return $this->respondWithError('File upload error');
            }
        }
    }

    /**
     * answer to poll
     *
     * @param Option $option
     * @return \Illuminate\Http\JsonResponse
     */
    public function optionAnswer(Option $option)
    {
        $user = auth()->user();
        $now = Carbon::now()->format('Y-m-d H:i:s');
        $duration = $option->post->duration;
        if ($now < $duration) {
            $optionsId = Option::where('post_id', $option->post_id)->pluck('id');
            $post = Post::whereId($option->post_id)->first();
            $post->touch();
            DB::table('option_user')->where('user_id', $user->id)->whereIn('option_id', $optionsId)->delete();
            DB::table('option_user')->insert([
                'option_id' => $option->id,
                'user_id' => $user->id
            ]);
            return $this->respondSuccess(new PostResource($option->post));
        } else {
            return $this->respondValidationError('voting is over', 'error');
        }
    }

    /**
     * counter a my posts
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function myPosts(User $user)
    {
        $all_posts = $user->post()->count();
        $posts = Post::where('user_id', $user->id)->whereIn('type', [Post::TYPE_PHOTO, Post::TYPE_VIDEO])->pluck('id');
        $media = File::whereIn('type', [File::FILE_VIDEO, File::FILE_PHOTO])->where('fileable_type', 'App\Models\Post')->whereIn('fileable_id', $posts)->count();
        $videos = File::whereType(File::FILE_VIDEO)->where('fileable_type', 'App\Models\Post')->whereIn('fileable_id', $posts)->count();
        $photos = File::whereType(File::FILE_PHOTO)->where('fileable_type', 'App\Models\Post')->whereIn('fileable_id', $posts)->count();
        $data = ['all_posts' => $all_posts, 'media' => $media, 'videos' => $videos, 'photos' => $photos];
        return response()->json(['status' => 'success', 'status_code' => 200, 'message' => 'counter', 'data' => $data]);
    }

    /**
     * my posts by type
     *
     * @param $type
     * @return mixed
     */
    public function posts($type, User $user)
    {
        $posts = Post::where('user_id', $user->id)->whereIn('type', [Post::TYPE_PHOTO, Post::TYPE_VIDEO])->pluck('id');
        switch ($type) {
            case 1:
                $data = File::orderby('id', 'desc')->where('fileable_type', 'App\Models\Post')->whereIn('fileable_id', $posts)->paginate(10);
                return $this->respondWithPagination($data, new Collection(VideoPhotoResource::collection($data)));
                break;
            case 2:
                $videos = File::orderby('id', 'desc')->whereType(File::FILE_VIDEO)->where('fileable_type', 'App\Models\Post')->whereIn('fileable_id', $posts)->paginate(10);
                return $this->respondWithPagination($videos, new Collection(PostVideoResource::collection($videos)));
                break;

            case 3:
                $photos = File::orderby('id', 'desc')->whereType(File::FILE_PHOTO)->where('fileable_type', 'App\Models\Post')->whereIn('fileable_id', $posts)->paginate(10);
                return $this->respondWithPagination($photos, new Collection(PostPhotoResource::collection($photos)));
                break;
        }
    }

    /**
     * comment list by post
     *
     * @param Post $post
     * @return mixed
     */
    public function comments(Post $post)
    {
        $comment = $post->comments()->orderBy('id', 'desc')->paginate(15);
        return $this->respondWithPagination($comment, new Collection(CommentResource::collection($comment)));
    }

    /**
     * like
     *
     * @param Post $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function reaction(Post $post)
    {
        DB::beginTransaction();
        if ($post->reactions()->where('user_id', auth()->id())->exists()) {
            $like = $post->reactions()->where('user_id', auth()->id())->first();
            $like->notifications()->delete();
            $post->reactions()->where('user_id', auth()->id())->delete();
            $post->update(['likes' => $post->reactions()->count()]);
        } else {
            $tokens = Device::where('user_id', $post->user_id)->get();
            if (count($tokens) != 0 && $post->user_id != auth()->user()->id) {
                $this->push($tokens, $post->id);
            }
            $like = $post->reactions()->create([
                'user_id' => auth()->id(),
                'reaction' => Reaction::REACTION_LIKE
            ]);
            $post->update(['likes' => $post->reactions()->count()]);
            if (auth()->user()->id != $post->user_id) {
                $this->notification($like, Notification::LIKE_POST, $post->user_id);
            }
        }
        DB::commit();
        return $this->respondSuccess(new PostResource($post));
    }

    /**
     * get a posts by user
     *
     * @param User $user
     * @return mixed
     */
    public function postByUser(User $user, Request $request)
    {
        return $this->filterByUser($user, $request);
    }

    public function update(Post $post, PostUpdateRequest $request)
    {
        DB::beginTransaction();
        if (!is_null($request->deletedFile)) {
            File::whereIn('id', $request->deletedFile)->delete();
        }
        if (!is_null($request->photos)) {
            $this->photo($request, $post);
        }
        if (!is_null($request->videos)) {
            $this->video($request, $post);
        }
        if (!is_null($request->price) && $request->price != $post->price) {
            $post->update($request->only('description', 'price'));
            $this->createProduct($post);

        }
        if (!is_null($request->pin)) {
            $user = User::find($post->user_id);
            $lastPinId = $user->post()->where('pin', Post::PIN)->first();
            if ($lastPinId) {
                $lastPin = Post::whereId($lastPinId->id)->first();
                $lastPin->update(['pin' => 0]);
            }
            $post->update($request->only('pin'));

        }
        $post->update($request->only('description', 'price'));

        DB::commit();
        return $this->respondSuccess(new PostResource($post));

    }

    public function updatePoll(Post $post, PollRequest $request)
    {
        if ($post->type == Post::TYPE_POLL) {
            DB::beginTransaction();

            $post->update($request->validated());
            $ids = array();
            foreach ($request->input('options') as $option) {
                if (array_key_exists('id', $option)) {
                    $ids[] = $option['id'];
                    $post->options()->updateOrCreate(
                        ['id' => $option['id']],
                        ['name' => $option['name']]
                    );
                }
            }
            $post->options()->whereNotIn('id', $ids)->delete();
            foreach ($request->input('options') as $option) {
                if (!array_key_exists('id', $option)) {
                    $post->options()->create($option);
                }
            }
            DB::commit();
            return $this->respondSuccess(new PostResource($post));
        } else {
            return $this->respondValidationError('it is not poll', 'error');
        }
    }

    /**
     *  delete a post
     *
     * @param Post $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Post $post)
    {
        $post->customlist()->delete();
        $post->bookmarks()->delete();
        $post->delete();
        return $this->respondSuccess(null, 'deleted');
    }

    /**
     * search a post
     *
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request, $type, User $user)
    {
        $followers = Network::where('subscriber_id', $user->id)->pluck('subscription_id')->toArray();
//        $users = array_merge($followers, [$user->id]);
        $now = Carbon::now()->format('Y-m-d H:i:s');
        if ($type == Post::TYPE_HOME) {
            $post = Post::orderBy('id', 'desc')->whereNull('schedule')->where('description', "LIKE", "$request->description%")
                ->orwhere('schedule', '<', $now)->where('description', "LIKE", "$request->description%")
                ->get()->toArray();
            $users = User::orderBy('id', 'desc')->where('id', '!=', $user->id)->where('username', "LIKE", "$request->description%")
                ->get()->toArray();
            $result = array_merge($users, $post);
//            return $result;
            $object = (object)$this->paginate($result, 10, null, $type, $user);
            return $this->respondWithPagination($object, new Collection(SearchResource::collection($object)));
        }
        if ($type == Post::TYPE_MY_FEED) {
            $post = $user->post()->orderBy('id', 'desc')->where('description', "LIKE", "$request->description%")->paginate(10);
            return $this->respondWithPagination($post, new Collection(PostResource::collection($post)));
        }
        if ($type == Post::TYPE_USER_FEED) {
            $post = $user->post()->orderBy('id', 'desc')->whereNull('schedule')->where('description', "LIKE", "$request->description%")
                ->orwhere('schedule', '<', $now)->where('description', "LIKE", "$request->description%")
                ->paginate(10);
            return $this->respondWithPagination($post, new Collection(PostResource::collection($post)));
        }
    }

    public function paginate($items, $perPage, $page = null, $type, $user)
    {
        $options = ['path' => url('api/post-search/' . $type . '/' . $user->id)];
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function feed(User $user, Request $request)
    {

        $followers = Network::where('subscriber_id', $user->id)->pluck('subscription_id')->toArray();
        $users = array_merge($followers, [$user->id]);
        $close_friends = Network::where('subscriber_id', $user->id)->where('close_friend', Network::CLOSE_FRIEND)->pluck('subscription_id')->toArray();
        $friends = array_merge($close_friends, [$user->id]);
        switch ($request->filter) {
            case 1:
                return $this->FilterFirst($user, $request);

//                return $this->respondWithPagination($post, new Collection(PostResource::collection($post)));
                break;
            case 2:
//                $post = Post::orderBy('id', 'desc')->whereIn('user_id', $friends)->whereNull('schedule')
//                    ->orwhere('schedule', '<', $now)->whereIn('user_id', $friends)
//                    ->paginate(10);
                return $this->feedTrait($friends, $request);

                break;
            case 3:
//                $post = Post::orderBy('id', 'desc')->whereIn('user_id', $users)->whereNull('schedule')
//                    ->orwhere('schedule', '<', $now)->whereIn('user_id', $users)
//                    ->paginate(10);
                return $this->feedTrait($users, $request);
                break;
            case 0:
                return $this->filterByUser($user, $request);

        }
//        return $this->respondWithPagination($post, new Collection(PostResource::collection($post)));

    }

    /**
     * my posts
     *
     * @param User $user
     * @return mixed
     */
    public function myPost(User $user, Request $request)
    {
        return $this->myFilter($user, $request);
    }

    public function report(ReportRequest $request)
    {
        $data = auth()->user()->report()->create([
            'post_id' => $request->post_id,

        ]);
        return $this->respondSuccess(new PostResource(Post::find($data->post_id)), 'reported');

    }

    public function tips(Post $post, Request $request)
    {
        \Stripe\Stripe::setApiKey(
            self::STRIPE_KEY
        );

        $payment = Payment::whereId($post->paymentable->id)->first();
        $id = $payment->history()->create([
            'user_id' => auth()->user()->id,
            'status' => History::STATUS_WAITING,
            'salesman_id' => $payment->paymentable->user_id,
            'type' => 'tips'
        ]);

        $customer = auth()->user()->customer->customer_id;

        $data = \Stripe\PaymentIntent::create([
            'payment_method_types' => ['card'],
            'amount' => $request->price * 100,
            'currency' => 'gbp',
            'confirm' => true,
            'customer' => "$customer",
            'description' => $id->id,
            'payment_method' => "$request->card_token",
            'metadata' => [
                'product_id' => $payment->product_id
            ]
        ]);
        return $this->respondSuccess(new PostResource($post));
    }

    public function createProduct($post)
    {
        $stripe = new \Stripe\StripeClient(
            self::STRIPE_KEY
        );

        $product = $stripe->products->create([
            'name' => "post: $post->id",
        ]);
        $price = $stripe->prices->create(
            [
                'product' => $product->id,
                'unit_amount_decimal' => $post->price * 100,
                'currency' => 'usd',
            ]
        );
        $post->paymentable()->create([
            'product_id' => $product->id,
            'price_id' => $price->id,
        ]);
        return $this->respondSuccess(new PostResource($post));
    }


}
