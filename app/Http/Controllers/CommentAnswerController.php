<?php

namespace App\Http\Controllers;

use App\Http\Requests\Comment\AnswerRequest;
use App\Http\Resources\Collection;
use App\Http\Resources\Comment\AnswerResource;
use App\Http\Traits\NotificationTrait;
use App\Http\Traits\PushNotificationTrait;
use App\Models\Comment;
use App\Models\Device;
use App\Models\Notification;
use App\Models\Reaction;
use App\Models\RecommendComment;
use DB;
use App\Models\CommentAnswer;
use App\Http\Requests\Comment\AnswerUpdateRequest;
use Exception;
use Illuminate\Http\JsonResponse;

class CommentAnswerController extends ApiController
{
    use PushNotificationTrait;
    use NotificationTrait;

    /**
     * replay to comment
     *
     * @param AnswerRequest $request
     * @return JsonResponse
     */
    public function store(AnswerRequest $request)
    {
        DB::beginTransaction();
        $comment = Comment::whereId($request->parent_id)->first();
        $answer = new CommentAnswer($request->all());
        $answer->user_id = auth()->id();
        $answer->save();
        if ($request->input('users')) {
            $answer->users()->sync($request->input('users'));
        }
        if (auth()->user()->id != $comment->user_id) {

            $this->notification($answer, Notification::ANSWER_COMMENT, $comment->user_id);
        }
        $tokens = Device::where('user_id', $comment->user_id)->get();
        if (count($tokens) != 0 &&  $comment->user_id != auth()->user()->id) {
            $this->pushComment($tokens,$comment->commentable_id);
        }
//
        DB::commit();
        return $this->respondCreated('Created', new AnswerResource($answer));
    }


    /**
     * show a answer
     *
     * @param CommentAnswer $answer
     * @return JsonResponse
     */
    public function show(CommentAnswer $answer)
    {
        return $this->respondSuccess(new AnswerResource($answer));
    }

    /**
     * update a answer
     *
     * @param AnswerUpdateRequest $request
     * @param CommentAnswer $answer
     * @return JsonResponse
     */
    public function update(AnswerUpdateRequest $request, CommentAnswer $answer)
    {
        DB::beginTransaction();
        $answer->update($request->only('message'));
        if ($request->input('users')) {
            $answer->users()->sync($request->input('users'));
        }
        DB::commit();
        return $this->respondCreated('Created', new AnswerResource($answer));
    }


    /**
     * delete a answer
     *
     * @param CommentAnswer $answer
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(CommentAnswer $answer)
    {
        $answer->delete();
        return $this->respondSuccess(null, 'Deleted');
    }

    /**
     * answers by comment
     *
     * @param Comment $comment
     * @return JsonResponse
     */
    public function index(Comment $comment)
    {
        $answers = CommentAnswer::where('parent_id', $comment->id)->with('user')->orderby('id', 'desc')->paginate(15);
        return $this->respondWithPagination($answers, new Collection(AnswerResource::collection($answers)));
    }

    /**
     * @param CommentAnswer $comment
     * @return JsonResponse
     */
    public function reaction(CommentAnswer $comment)
    {
        DB::beginTransaction();
        if ($comment->reactions()->where('user_id', auth()->id())->exists()) {
            $like = $comment->reactions()->where('user_id', auth()->id())->first();
            $like->notifications()->delete();
            $comment->reactions()->where('user_id', auth()->id())->delete();
        } else {
            $tokens = Device::where('user_id', $comment->user_id)->get();
            if (count($tokens) != 0 && $comment->user_id != auth()->user()->id) {
                $this->push($tokens,$comment->comment->commentable_id);
            }
            $like = $comment->reactions()->create([
                'user_id' => auth()->id(),
                'reaction' => Reaction::REACTION_LIKE
            ]);
            if (auth()->user()->id != $comment->user_id) {
                $this->notification($like, Notification::LIKE_ANSWER, $comment->user_id);
            }
        }
        DB::commit();
        return $this->respondSuccess(new AnswerResource($comment));
    }

    protected function VariableIds($comment)
    {
        switch ($comment->comment->commentable_type) {
            case "App\Models\Recommend":
                $recommend_id = $comment->comment->commentable_id;
                $post_id = null;
                break;
            case "App\Models\Post":
                $post_id = $comment->comment->commentable_id;
                $recommend_id = null;
                break;
            case "App\Models\RecommendRating":
                $recommend_id = $comment->comment->commentable->recommend_id;
                $post_id = null;
                break;
        }
        return [$post_id, $recommend_id];
    }
}
