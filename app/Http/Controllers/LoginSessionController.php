<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginSession\LoginSessionRequest;
use App\Http\Resources\Collection;
use App\Http\Resources\LoginSession\LoginSessionResource;
use App\Models\LoginSession;
use Illuminate\Http\Request;

class LoginSessionController extends ApiController
{

    public function store(LoginSessionRequest $request)
    {
        $data = auth()->user()->loginSession()->create($request->all());
        return $this->respondSuccess($data, 'login session added');
    }

    public function delete(LoginSession $session)
    {
        $session->delete();
        return $this->respondSuccess(null, 'login session deleted');
    }

    public function mySessions(LoginSession $session)
    {

        $data = auth()->user()->loginSession()->pluck('id')->toArray();
        array_unshift($data, $session->id);
        $sessions = LoginSession::whereIn('id', $data)->paginate(10);

        $sessions->setCollection(
            $sessions->sortBy(function ($loginSession, $key) use ($data) {
                return array_search($loginSession->id, $data);
            })
        );
        return $this->respondWithPagination($sessions, new Collection(LoginSessionResource::collection($sessions)));
    }

    public function removeAll(LoginSession $session)
    {
        auth()->user()->loginSession()->where('id', '!=', $session->id)->delete();
        return $this->respondSuccess(null, 'login session deleted');

    }

}
