<?php

namespace App\Http\Controllers;

use App\Http\Requests\Bundle\BundleDeleteRequest;
use App\Http\Requests\Bundle\BundleRequest;
use App\Http\Requests\User\PriceRequest;
use App\Http\Resources\Collection;
use App\Http\Resources\Bundle\DataResource;
use App\Http\Resources\Bundle\BundleResource;
use App\Models\Bundle;
use App\Models\Discount;
use App\Models\Duration;
use App\Models\History;
use App\Models\Payment;
use App\Models\Promotion;
use App\Models\Subscription;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Symfony\Component\VarDumper\Cloner\Data;

class BundleController extends ApiController
{
    const STRIPE_KEY = "sk_test_51KACWHLoIcQRAFhtAXVE7euhvmvWBd2s7uCU3bAGKn0M34HwUf5PSKp3R2nIg4fO0A0FNJaZeHg9iwb3ZY1RBbou0099UAZRat";

    public function price(PriceRequest $request)
    {
        $stripe = new \Stripe\StripeClient(
            self::STRIPE_KEY
        );

        $user = auth()->user();
        $user->update(['subscription_price' => $request->subscription_price * 100]);

        $product = $stripe->products->create([
            'name' => 'User Subscription',
        ]);
        $price = $stripe->prices->create(
            [
                'product' => $product->id,
                'unit_amount_decimal' => $user->subscription_price,
                'currency' => 'gbp',
                'recurring' => ['interval' => 'month'],
            ]
        );
        $user->paymentable()->create([
            'product_id' => $product->id,
            'price_id' => $price->id,
        ]);
        return $this->respondSuccess($user->subscription_price / 100);
    }

    public function store(BundleRequest $request)
    {

        $user = auth()->user();

        if ($user->bundles()->where('status', Bundle::STATUS_ACTIVE)->count() >= 3) {
            return $this->respondValidationError('Max 3 bundles', 422);
        } else {

            $duration = ceil(Duration::whereId($request->duration_id)->first()->title);
            $percent = ceil(Discount::whereId($request->discount_id)->first()->title) / 100;
            $discount_sum = $user->subscription_price * $percent;
            if ($discount_sum >= 25000) {
                return $this->respondValidationError('Max discount 250', 422);
            }
            $stripe = new \Stripe\StripeClient(
                self::STRIPE_KEY
            );

            $product = $stripe->products->create([
                'name' => 'Bundle',
            ]);
            $price = $stripe->prices->create(
                [
                    'product' => $product->id,
                    'unit_amount_decimal' => ($user->subscription_price - ($user->subscription_price * $percent)) * $duration,
                    'currency' => 'gbp',
                ]
            );
            $data = $user->bundles()->create([
                'duration_id' => $request->duration_id,
                'discount_id' => $request->discount_id,
                'price' => ($user->subscription_price - ($user->subscription_price * $percent)) * $duration,
                'status' => Bundle::STATUS_ACTIVE
            ]);
            $data->paymentable()->create([
                'product_id' => $product->id,
                'price_id' => $price->id,
            ]);

            return $this->respondSuccess(new BundleResource(Bundle::find($data->id)));
        }
    }

    public function index()
    {
        $bundles = auth()->user()->bundles()->where('status', Bundle::STATUS_ACTIVE)->get();
        return $this->respondSuccess(new Collection(BundleResource::collection($bundles)));
    }

    public function durations()
    {
        return $this->respondSuccess(Duration::all());
    }

    public function discounts()
    {
        return $this->respondSuccess(Discount::all());
    }

    public function destroy(BundleDeleteRequest $request, Bundle $bundle)
    {

        $bundle->update(['status' => Bundle::STATUS_INACTIVE]);
        return $this->respondSuccess(null, 'success deleted');
    }

    public function bundles(User $user)
    {
//        $data = Payment::where('user_id',$user->id)->get();
//        return $this->respondSuccess(new Collection(PaymentResource::collection($data)));


        return $this->respondSuccess(new DataResource($user));
    }

    public function sub()
    {
        \Stripe\Stripe::setApiKey('sk_test_51KACWHLoIcQRAFhtAXVE7euhvmvWBd2s7uCU3bAGKn0M34HwUf5PSKp3R2nIg4fO0A0FNJaZeHg9iwb3ZY1RBbou0099UAZRat');

// The price ID passed from the front end.
//   $priceId = $_POST['priceId'];
        $priceId = 'price_1KLR0CLoIcQRAFhtAoqrqRS3';

        $session = \Stripe\Checkout\Session::create([
            'success_url' => url('api/successPayment'),
            'cancel_url' => url('api/successPayment'),
            'mode' => 'subscription',
            'line_items' => [[
                'price' => $priceId,
                // For metered billing, do not pass quantity
                'quantity' => 1,
            ]],
        ]);
return response()->json($session);
    }

}
