<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactUs\ContactUsRequest;
use App\Models\ContactUs;
use Illuminate\Http\Request;

class ContactUsController extends ApiController
{
    public function store(ContactUsRequest $request)
    {
        $contactUs = new ContactUs($request->all());
        $contactUs->save();
        return $this->respondSuccess($contactUs,'sms sent');

    }
}
