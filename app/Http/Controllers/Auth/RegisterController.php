<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\RegisterRequest;
use App\Http\Resources\User\RegisterResource;
use App\Http\Resources\User\UserResource;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    public function register(RegisterRequest $request)
    {
        DB::beginTransaction();
        $user = new User($request->all());
        $user->password = Hash::make($request->password);
        $user->save();
        $user->notifications()->create();
        $token = JWTAuth::fromUser($user);
        DB::commit();

        return response()->json(['status' => true, 'message' => 'success register', 'data' => new RegisterResource($user), 'token' => $token]);
    }
}
