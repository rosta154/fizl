<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\LoginRequest;
use App\Http\Resources\User\UserResource;
use App\Http\Traits\SmsTrait;
use App\Models\User;
use App\Models\UserDevice;
use App\Models\Verification;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use JWTAuth;

class LoginController extends Controller
{
    use SmsTrait;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = "/users";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginUser(LoginRequest $request)
    {
        $token = JWTAuth::attempt($request->validated());
        $user = User::whereUsername($request->username)->first();
        if (!$user) {
            return response()->json(['message' => 'User not found'], 404);
        }
        if ($user->two_step_sms == 0) {
            if (!$token) {
                return response()->json(['message' => 'User not found'], 404);
            }
            return response()->json(['status' => true, 'message' => 'success login', 'data' => new UserResource(auth()->user()), 'token' => $token]);
        } else {
            if (!$token) {
                return response()->json(['message' => 'User not found'], 404);
            }
            $devices = UserDevice::where('user_id', $user->id)->pluck('device_id')->toArray();
            if (!is_null($request->device_id) && in_array($request->device_id, $devices)) {
                return response()->json(['status' => true, 'message' => 'success login', 'data' => new UserResource(auth()->user()), 'token' => $token]);
            }
            return $this->sendCode($user);
//            return response()->json(['message' => 'SMS with the code has been sent to you.']);
        }
    }

    /**
     * send code
     *
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendCode($user)
    {
        $phone = $user->phone;
        if ($phone == null) {
            return response()->json(['message' => 'Phone not found'], 404);
        }
        $code = $this->generateCode();
        Verification::updateOrCreate(['phone' => $phone], ['code' => $code]);
        $send = $this->sendSms($phone, $code);
        return $send;
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        $credentials = $request->only('email', 'password');
        $check = User::where('email', $credentials['email'])
            ->where('role', 'ADMIN')
            ->first();
        if (!$check) {
            return view('auth.login');
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
}
