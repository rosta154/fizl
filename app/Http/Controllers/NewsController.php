<?php

namespace App\Http\Controllers;

use App\Http\Resources\Collection;
use App\Http\Resources\News\NewsResource;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends ApiController
{
    public function index()
    {
        $news = News::orderBy('id', 'desc')->paginate(10);
        return $this->respondWithPagination($news, new Collection(NewsResource::collection($news)));
    }
}

