<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\User;
use App\Models\UserVerification;
use Illuminate\Http\Request;
use Storage;
class UserController extends Controller
{
    public function index()
    {
        $users = User::where('verify', User::VERIFY_PENDING)->simplePaginate(10);
        return view('users.index', compact('users'));
    }

    public function show(User $user)
    {
        $ids = UserVerification::where('user_id', $user->id)->pluck('id');
        $photos = File::where('type', File::FILE_DOCUMENT)->whereIn('fileable_id', $ids)->get();
        return view('users.photos', compact('photos', 'user'));
    }

    public function approved(User $user)
    {
        $user->update(['verify' => User::VERIFY]);
        return redirect()->route('users.index');
    }

    public function reject(User $user)
    {
        $user->update(['verify' => 0]);
        $files = Storage::allFiles('public/user-docs/' . $user->id . '/');
        if ($files) {
            Storage::delete($files);
            $ids = UserVerification::where('user_id', $user->id)->pluck('id');
            File::where('type', File::FILE_DOCUMENT)->whereIn('fileable_id', $ids)->delete();
        }
        return redirect()->route('users.index');
    }
}
