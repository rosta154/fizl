<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\DeleteAccountRequest;
use App\Models\Comment;
use App\Models\Notification;
use App\Models\UserDelete;
use Illuminate\Http\Request;

class UserDeleteController extends Controller
{

    public function sendCode()
    {
        $user = auth()->user();
        $code = $this->generateCode();
        $email = $user->email;
        UserDelete::updateOrCreate(['user_id' => $user->id], ['code' => $code]);
        $send = $this->sendSms($email, $code);
        return $send;
    }

    protected function generateCode()
    {
        $code = mt_rand(1111, 9999);
        return $code;
    }

    public function sendSms($to, $code)
    {
        return response()->json(['status' => true, 'message' => 'SMS with the code has been sent to you.', 'code' => $code]);
//        return response()->json(['code' => 0, 'message' => $code]);
//        return response()->json(['code' => 0, 'message' => 'Смс с кодом отправлено']);
    }

    public function verifyCode(DeleteAccountRequest $request)
    {
        $user = auth()->user();
        $checkVerification = UserDelete::where('user_id', $user->id)->where('code', $request->code)->exists();
        if (!$checkVerification) {
            return response()->json(['message' => 'Code not right'], 404);
        } else {
            UserDelete::where('user_id', $user->id)->where('code', $request->code)->delete();
            $this->deleteAccount($user);
            return response()->json(['message' => 'Account deleted'], 200);
        }
    }

    public function deleteAccount($user)
    {
        $user->reactions()->delete();
        Notification::where('sender_id', $user->id)->delete();
        Comment::whereIn('commentable_id', $user->post()->pluck('id'))->delete();
        $user->answer_comments()->delete();
        $user->comments()->delete();
        $user->post()->delete();
        $user->delete();

    }
}
