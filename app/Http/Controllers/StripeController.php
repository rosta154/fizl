<?php

namespace App\Http\Controllers;

use App\Http\Requests\Bundle\UserSubscriptionRequest;
use App\Models\Bundle;
use App\Models\History;
use App\Models\Payment;
use App\Models\Promotion;
use App\Models\Subscription;
use App\Models\User;
use App\Models\UserBalance;
use App\Models\UserBank;
use App\Models\UserCard;
use App\Models\UserSubscription;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StripeController extends ApiController
{

    const STRIPE_KEY = "sk_test_51KACWHLoIcQRAFhtAXVE7euhvmvWBd2s7uCU3bAGKn0M34HwUf5PSKp3R2nIg4fO0A0FNJaZeHg9iwb3ZY1RBbou0099UAZRat";

    public function saveCard(Request $request)
    {

        \Stripe\Stripe::setApiKey(
            self::STRIPE_KEY
        );
        $user = auth()->user();
        $check = UserCard::where('token', $request->source)->where('user_id', $user->id)->exists();
        if (!$check) {
            $check2 = UserCard::where('user_id', $user->id)->exists();
            if ($check2) {


                $stripe = new \Stripe\StripeClient(
                    self::STRIPE_KEY
                );
                $customer = $user->customer->customer_id;
                $data = $stripe->customers->createSource(
                    "$customer",
                    ['source' => "$request->source"]
                );
                UserCard::create([
                    'user_id' => $user->id,
                    'token' => $request->source,
                    'card_token' => $data->id,
                    'customer_id' => $user->customer->customer_id,
                    'number' => $request->number,
                ]);
            } else {
                $stripe = new \Stripe\StripeClient(
                    self::STRIPE_KEY
                );
            $data =   $stripe->accounts->create([
                    'type' => 'express',
                    'country' => 'US',
                    'email' => 'jenny.rosen@example.com',
                    'capabilities' => [
                        'card_payments' => ['requested' => true],
                        'transfers' => ['requested' => true],
                    ],
                ]);

            return  response()->json($data);
//                 $stripe->accounts->update(
//                    "$data->id",
//                    ['tos_acceptance' => ['date' => 1609798905, 'ip' => '8.8.8.8']]
//                );
//                 $customer = \Stripe\Customer::create([
//                    'source' => $request->source,
//                    'email' => $user->email,
//                    'name' => $user->name
//                ]);
//
//                UserCard::create([
//                    'user_id' => $user->id,
//                    'token' => $request->source,
//                    'card_token' => $customer->default_source,
//                    'customer_id' => $customer->id,
//                    'number' => $request->number,
//                ]);
            }
            return response()->json(['success' => true, 'message' => 'card added']);
        } else {
            return response()->json(['success' => false, 'message' => 'customer already exists']);
        }
    }

    public function addBank(Request $request)
    {
        $stripe = new \Stripe\StripeClient(
            self::STRIPE_KEY
        );

        $check = UserBank::where('token', $request->source)->exists();
        if (!$check) {
            $user = auth()->user();
            $bank = $stripe->customers->createSource(
                "$request->customer_id",
                ['source' => "$request->source"]
            );
            UserBank::create([
                'user_id' => $user->id,
                'token' => $request->source,
                'customer_id' => $request->customer_id,
                'number' => $request->number,
            ]);
            return response()->json($bank);
        } else {
            return response()->json(['code' => 1, 'message' => 'bank already exists']);
        }

    }

    public function verifyCard(Request $request)
    {
        $stripe = new \Stripe\StripeClient(
            self::STRIPE_KEY
        );
        $test = $stripe->customers->verifySource(
            "$request->customer_id",
            "$request->source",
            ['amounts' => [32, 45]]
        );
        return response()->json($test);
    }


    public function product()
    {
        $stripe = new \Stripe\StripeClient(
            self::STRIPE_KEY
        );
        $test = $stripe->products->create([
            'name' => 'Gold Special',
//           'amount' => 40
        ]);


        $price = $stripe->prices->create(
            [
                'product' => $test->id,
                'unit_amount' => 1000,
                'currency' => 'gbp',
            ]
        );
        return response()->json($price);

    }

    public function pay(UserSubscriptionRequest $request)
    {
        \Stripe\Stripe::setApiKey(
            self::STRIPE_KEY
        );
        header('Content-Type: application/json');

        $payment = Payment::whereId($request->payment_id)->first();
        $user = auth()->user();
        $history = $payment->history()->create([
            'user_id' => $user->id,
            'status' => Subscription::STATUS_WAITING,
        ]);

        return $this->payment($payment, $history);
    }


    public function success()
    {

//        $subscription = UserSubscription::whereId($payment)->first();
//        if ($subscription) {
//            $subscription->update(['status' => Subscription::STATUS_APPROVED]);
        return 'success';
//        } else {
//            return 'error';
//        }
    }

    public function check()
    {
        \Stripe\Stripe::setApiKey(
            self::STRIPE_KEY
        );

        $endpoint_secret = 'whsec_LTSCqFwX8uQMNhrbqQDo8CnXdSPnw2v4';
        $payload = @file_get_contents('php://input');
        $event = null;
        try {
            $event = \Stripe\Event::constructFrom(
                json_decode($payload, true)
            );
        } catch (\UnexpectedValueException $e) {
            // Invalid payload
            echo '⚠️  Webhook error while parsing basic request.';
            http_response_code(400);
            exit();
        }
        if ($endpoint_secret) {
            // Only verify the event if there is an endpoint secret defined
            // Otherwise use the basic decoded event
            $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
            try {
                $event = \Stripe\Webhook::constructEvent(
                    $payload, $sig_header, $endpoint_secret
                );
            } catch (\Stripe\Exception\SignatureVerificationException $e) {
                // Invalid signature
                echo '⚠️  Webhook error while validating signature.';
                http_response_code(400);
                exit();
            }
        }

// Handle the event
        switch ($event->type) {

            case 'payment_intent.succeeded':
                $session = $event->data->object;
                $subscription = \App\Models\History::whereId($session->description)->first();
                $duration = $subscription->payment->paymentable->duration;
                $expiration = $subscription->payment->paymentable->expiration;
                $salesman = User::whereId($subscription->salesman_id)->first();

                if ($duration) {
                    $subscription->update(['end_date' => Carbon::now()->addMonths(ceil($duration->title))->format('Y-m-d H:i:s'),
                        'status' => History::STATUS_APPROVED]);
                    $salesman->update(['balance' => $salesman->balance + $session->amount]);
                } elseif ($expiration) {
                    $expiration = $subscription->payment->paymentable->expiration;
                    $subscription->update(['end_date' => Carbon::now()->addMonths(ceil($expiration->title))->format('Y-m-d H:i:s'),
                        'status' => History::STATUS_APPROVED]);
                    $salesman->update(['balance' => $salesman->balance + $session->amount]);
                } else {
                    $salesman->update(['balance' => $salesman->balance + $session->amount]);
                    $subscription->update(['status' => History::STATUS_APPROVED]);
                }
                break;
            case 'payment_intent.created':
//                $session = $event->data->object;
//                $subscription = \App\Models\History::whereId($session->client_reference_id)->first();
//                $subscription->update(['status' => \App\Models\Subscription::STATUS_APPROVED]);
                return 'created';
                break;
            case 'payment_intent.payment_failed':
                $session = $event->data->object;
                $subscription = \App\Models\History::whereId($session->description)->first();
                $subscription->update(['status' => \App\Models\Subscription::STATUS_WAITING]);
                echo 'success';
                break;
            default:
                // Unexpected event type
                error_log('Received unknown event type');
        }

        http_response_code(200);
    }

    public function cancel()
    {
        return 'cancel';
    }

    public function cardToken(Request $request)
    {
        $stripe = new \Stripe\StripeClient(
            self::STRIPE_KEY
        );
        $token = $stripe->tokens->create([
            'card' => [
                'number' => "$request->number",
                'exp_month' => $request->exp_month,
                'exp_year' => $request->exp_year,
                'cvc' => $request->cvc,
            ],
        ]);
        return response()->json($token);
    }

    public function payToken(Request $request)
    {
        \Stripe\Stripe::setApiKey(
            self::STRIPE_KEY
        );

        $payment = Payment::whereId($request->payment_id)->first();
        $price = $payment->price_id;
        $stripe = new \Stripe\StripeClient(
            self::STRIPE_KEY
        );
        $amount = $stripe->prices->retrieve(
            "$price",
            []
        );
        $customer = auth()->user()->customer->customer_id;
        if ($payment->paymentable_type == 'App\Models\User') {
            $id = $payment->history()->create([
                'user_id' => auth()->user()->id,
                'status' => History::STATUS_WAITING,
                'salesman_id' => $payment->paymentable->id,
            ]);
            $data = \Stripe\PaymentIntent::create([
                'payment_method_types' => ['card'],
                'amount' => $amount->unit_amount_decimal,
                'currency' => 'gbp',
                'confirm' => true,
                'customer' => "$customer",
                'description' => $id->id,
                'payment_method' => "$request->card_token",
                'setup_future_usage' => 'off_session',
                'metadata' => [
                    'price_id' => "$price",
                ],
            ]);
        } else {
            $id = $payment->history()->create([
                'user_id' => auth()->user()->id,
                'status' => History::STATUS_WAITING,
                'salesman_id' => $payment->paymentable->user_id,
                'type' => $payment->paymentable_type
            ]);
            $data = \Stripe\PaymentIntent::create([
                'payment_method_types' => ['card'],
                'amount' => $amount->unit_amount_decimal,
                'currency' => 'gbp',
                'confirm' => true,
                'customer' => "$customer",
                'description' => $id->id,
                'payment_method' => "$request->card_token",
            ]);
        }


        return $this->respondSuccess(null, 'created');

    }

    public function payment($object, $history)
    {
        $price = $object->price_id;
        $data = $checkout_session = \Stripe\Checkout\Session::create([
            'line_items' => [[
                # Provide the exact Price ID (e.g. pr_1234) of the product you want to sell
                'price' => $price,
                'quantity' => 1,
            ]],
            'client_reference_id' => $history->id,
            'mode' => 'payment',
//            'customer' => $user->customer->customer_id,
            'success_url' => url('/api/successPayment'),
            'cancel_url' => url('/api/cancelPayment'),

        ]);
        header("HTTP/1.1 303 See Other");
        header("Location: " . $checkout_session->url);
        return response()->json($data);
    }

    public function payouts()
    {
        $stripe = new \Stripe\StripeClient(
            self::STRIPE_KEY
        );
////        $data = $stripe->balance->retrieve();
//        $data = $stripe->payouts->create([
//            'amount' => 900,
//            'currency' => 'usd',
////            'destination' => 'cus_L1rg8X2wi8d4rN',
//            'source_type' => 'card'
//        ]);
   $data =     $stripe->accounts->create([
            'type' => 'express',
            'country' => 'US',
//            'client_id' => 'ca_L490KnKbwvxF3PGVPrms3lB7n5xbIRPz',
            'email' => 'jenny.rosen@example.com',
            'business_type' => 'individual',
            'individual' => ['dob' =>[
                'day'=> '01',
                'month'=> '01',
                'year'=> '1901',

            ]],
            'capabilities' => [
                'card_payments' => ['requested' => true],
                'transfers' => ['requested' => true],
            ],
        ]);

        return response()->json($data);
    }
}

