<?php


namespace App\Http\Traits;


use App\Http\Resources\Collection;
use App\Http\Resources\Post\PostResource;
use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

trait PostTrait
{

    public function myFilter($user, $request)
    {
        $last_3_months = Carbon::now()->subMonths(3)->format('Y-m-d');
        $last_month = Carbon::now()->subMonth()->format('Y-m-d');
        $last_week = Carbon::now()->subWeek()->format('Y-m-d');

        $pin = $user->post()->where('pin', Post::PIN)->pluck('id')->toArray();

        $postDesc = $user->postDesc()->pluck('id')->toArray();
        $allPostDescIds = array_merge($pin, $postDesc);
        $allPostDesc = array_values(array_unique($allPostDescIds));
        $postsDesc = Post::whereIn('id', $allPostDesc)->get()
            ->sortBy(function ($post, $key) use ($allPostDesc) {
                return array_search($post->id, $allPostDesc);
            });


        $postAsc = $user->postAsc()->pluck('id')->toArray();
        $allPostAscIds = array_merge($pin, $postAsc);
        $allPostAsc = array_values(array_unique($allPostAscIds));
        $postsAsc = Post::whereIn('id', $allPostAsc)->get()
            ->sortBy(function ($post, $key) use ($allPostAsc) {
                return array_search($post->id, $allPostAsc);
            });

        $postDescLikes = $user->postDescLikes()->pluck('id')->toArray();
        $allPostDescLikesIds = array_merge($pin, $postDescLikes);
        $allPostDescLikes = array_values(array_unique($allPostDescLikesIds));
        $postsDescLikes = Post::whereIn('id', $allPostDescLikes)->get()
            ->sortBy(function ($post, $key) use ($allPostDescLikes) {
                return array_search($post->id, $allPostDescLikes);
            });

        $postAscLikes = $user->postAscLikes()->pluck('id')->toArray();
        $allPostAscLikesIds = array_merge($pin, $postAscLikes);
        $allPostAscLikes = array_values(array_unique($allPostAscLikesIds));
        $postsAscLikes = Post::whereIn('id', $allPostAscLikes)->get()
            ->sortBy(function ($post, $key) use ($allPostAscLikes) {
                return array_search($post->id, $allPostAscLikes);
            });

        $time = null;
        switch ($request->timeFilter) {
            case 0:
                $time = $last_3_months;
                break;
            case 1:
            case 2:
                $time = $last_3_months;
                break;
            case 3:
                $time = $last_month;
                break;
            case 4:
                $time = $last_week;
                break;
        }

        $postDescTime = $user->postDesc()->pluck('id')->toArray();
        $allPostDescTimeIds = array_merge($pin, $postDescTime);
        $allPostDescTime = array_values(array_unique($allPostDescTimeIds));
        $postsDescTime = Post::whereDate('created_at', '>', $time)->whereIn('id', $allPostDescTime)->get()
            ->sortBy(function ($post, $key) use ($allPostDescTime) {
                return array_search($post->id, $allPostDescTime);
            });

        $postAscTime = $user->postAsc()->pluck('id')->toArray();
        $allPostAscTimeIds = array_merge($pin, $postAscTime);
        $allPostAscTime = array_values(array_unique($allPostAscTimeIds));
        $postsAscTime = Post::whereDate('created_at', '>', $time)->whereDate('created_at', '>', $time)->whereIn('id', $allPostAscTime)->get()
            ->sortBy(function ($post, $key) use ($allPostAscTime) {
                return array_search($post->id, $allPostAscTime);
            });

        $postDescLikes3month = $user->postDescLikes()->pluck('id')->toArray();
        $allPostDescLikes3monthIds = array_merge($pin, $postDescLikes3month);
        $allPostDesc3monthLikes = array_values(array_unique($allPostDescLikes3monthIds));
        $postsDescLikesTime = Post::whereDate('created_at', '>', $time)->whereIn('id', $allPostDesc3monthLikes)->get()
            ->sortBy(function ($post, $key) use ($allPostDesc3monthLikes) {
                return array_search($post->id, $allPostDesc3monthLikes);
            });

        $postAscLikes3month = $user->postAscLikes()->pluck('id')->toArray();
        $allPostAscLikes3monthIds = array_merge($pin, $postAscLikes3month);
        $allPostAscLikes3month = array_values(array_unique($allPostAscLikes3monthIds));
        $postsAscLikesTime = Post::whereDate('created_at', '>', $time)->whereIn('id', $allPostAscLikes3month)->get()
            ->sortBy(function ($post, $key) use ($allPostAscLikes3month) {
                return array_search($post->id, $allPostAscLikes3month);
            });

        switch ($request->timeFilter) {
            case 0:
                $data = (object)$this->paginator($postsDesc, 10, null, null, $user);

                return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
            case 1:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = (object)$this->paginator($postsAsc, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = (object)$this->paginator($postsDesc, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = (object)$this->paginator($postsAscLikes, 10, null, null, $user);

                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = (object)$this->paginator($postsDescLikes, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 2:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = (object)$this->paginator($postsAscTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = (object)$this->paginator($postsDescTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = (object)$this->paginator($postsAscLikesTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = (object)$this->paginator($postsDescLikesTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 3:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = (object)$this->paginator($postsAscTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = (object)$this->paginator($postsDescTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = (object)$this->paginator($postsAscLikesTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = (object)$this->paginator($postsDescLikesTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 4:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = (object)$this->paginator($postsAscTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = (object)$this->paginator($postsDescTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = (object)$this->paginator($postsAscLikesTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = (object)$this->paginator($postsDescLikesTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
        }
    }

    public function filterByUser($user, $request)
    {
        $now = Carbon::now()->format('Y-m-d H:i:s');
        $last_3_months = Carbon::now()->subMonths(3)->format('Y-m-d');
        $last_month = Carbon::now()->subMonth()->format('Y-m-d');
        $last_week = Carbon::now()->subWeek()->format('Y-m-d');

        $pin = $user->post()->where('pin', Post::PIN)->pluck('id')->toArray();
        $postDesc = $user->postDesc()->pluck('id')->toArray();


        $allPostDescIds = array_merge($pin, $postDesc);
        $allPostDesc = array_values(array_unique($allPostDescIds));
        $postsDesc = Post::whereNull('schedule')->whereIn('id', $allPostDesc)->orwhere('schedule', '<', $now)->whereIn('id', $allPostDesc)->get()
            ->sortBy(function ($post, $key) use ($allPostDesc) {
                return array_search($post->id, $allPostDesc);
            });


        $postAsc = $user->postAsc()->pluck('id')->toArray();
        $allPostAscIds = array_merge($pin, $postAsc);
        $allPostAsc = array_values(array_unique($allPostAscIds));
        $postsAsc = Post::whereNull('schedule')->whereIn('id', $allPostAsc)->orwhere('schedule', '<', $now)->whereIn('id', $allPostAsc)->get()
            ->sortBy(function ($post, $key) use ($allPostAsc) {
                return array_search($post->id, $allPostAsc);
            });

        $postDescLikes = $user->postDescLikes()->pluck('id')->toArray();
        $allPostDescLikesIds = array_merge($pin, $postDescLikes);
        $allPostDescLikes = array_values(array_unique($allPostDescLikesIds));
        $postsDescLikes = Post::whereNull('schedule')->whereIn('id', $allPostDescLikes)->orwhere('schedule', '<', $now)->whereIn('id', $allPostDescLikes)->get()
            ->sortBy(function ($post, $key) use ($allPostDescLikes) {
                return array_search($post->id, $allPostDescLikes);
            });

        $postAscLikes = $user->postAscLikes()->pluck('id')->toArray();
        $allPostAscLikesIds = array_merge($pin, $postAscLikes);
        $allPostAscLikes = array_values(array_unique($allPostAscLikesIds));
        $postsAscLikes = Post::whereNull('schedule')->whereIn('id', $allPostAscLikes)->orwhere('schedule', '<', $now)->whereIn('id', $allPostAscLikes)->get()
            ->sortBy(function ($post, $key) use ($allPostAscLikes) {
                return array_search($post->id, $allPostAscLikes);
            });

        $time = null;
        switch ($request->timeFilter) {
            case 0:
                $time = $last_3_months;
                break;
            case 1:
                $time = $last_month;
                break;
            case 2:
                $time = $last_3_months;
                break;
            case 3:
                $time = $last_month;
                break;
            case 4:
                $time = $last_week;
                break;
        }

        $postDescTime = $user->postDesc()->pluck('id')->toArray();
        $allPostDescTimeIds = array_merge($pin, $postDescTime);
        $allPostDescTime = array_values(array_unique($allPostDescTimeIds));
        $postsDescTime = Post::whereDate('created_at', '>', $time)->whereNull('schedule')->whereIn('id', $allPostDescTime)->orwhere('schedule', '<', $now)->whereDate('created_at', '>', $time)->whereIn('id', $allPostDescTime)->get()
            ->sortBy(function ($post, $key) use ($allPostDescTime) {
                return array_search($post->id, $allPostDescTime);
            });

        $postAscTime = $user->postAsc()->pluck('id')->toArray();
        $allPostAscTimeIds = array_merge($pin, $postAscTime);
        $allPostAscTime = array_values(array_unique($allPostAscTimeIds));
        $postsAscTime = Post::whereDate('created_at', '>', $time)->whereNull('schedule')->whereIn('id', $allPostAscTime)->orwhere('schedule', '<', $now)->whereDate('created_at', '>', $time)->whereIn('id', $allPostAscTime)->get()
            ->sortBy(function ($post, $key) use ($allPostAscTime) {
                return array_search($post->id, $allPostAscTime);
            });

        $postDescLikes3month = $user->postDescLikes()->pluck('id')->toArray();
        $allPostDescLikes3monthIds = array_merge($pin, $postDescLikes3month);
        $allPostDesc3monthLikes = array_values(array_unique($allPostDescLikes3monthIds));
        $postsDescLikesTime = Post::whereDate('created_at', '>', $time)->whereNull('schedule')->whereIn('id', $allPostDesc3monthLikes)->orwhere('schedule', '<', $now)->whereDate('created_at', '>', $time)->whereIn('id', $allPostDesc3monthLikes)->get()
            ->sortBy(function ($post, $key) use ($allPostDesc3monthLikes) {
                return array_search($post->id, $allPostDesc3monthLikes);
            });

        $postAscLikes3month = $user->postAscLikes()->pluck('id')->toArray();
        $allPostAscLikes3monthIds = array_merge($pin, $postAscLikes3month);
        $allPostAscLikes3month = array_values(array_unique($allPostAscLikes3monthIds));
        $postsAscLikesTime = Post::whereDate('created_at', '>', $time)->whereNull('schedule')->whereIn('id', $allPostAscLikes3month)->orwhere('schedule', '<', $now)->whereDate('created_at', '>', $time)->whereIn('id', $allPostAscLikes3month)->get()
            ->sortBy(function ($post, $key) use ($allPostAscLikes3month) {
                return array_search($post->id, $allPostAscLikes3month);
            });


        switch ($request->timeFilter) {
            case 0:
                $data = (object)$this->paginator($postsDesc, 10, null, null, $user);

                return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
            case 1:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = (object)$this->paginator($postsAsc, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = (object)$this->paginator($postsDesc, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = (object)$this->paginator($postsAscLikes, 10, null, null, $user);

                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = (object)$this->paginator($postsDescLikes, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 2:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = (object)$this->paginator($postsAscTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = (object)$this->paginator($postsDescTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = (object)$this->paginator($postsAscLikesTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = (object)$this->paginator($postsDescLikesTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 3:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = (object)$this->paginator($postsAscTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = (object)$this->paginator($postsDescTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = (object)$this->paginator($postsAscLikesTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = (object)$this->paginator($postsDescLikesTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 4:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = (object)$this->paginator($postsAscTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = (object)$this->paginator($postsDescTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = (object)$this->paginator($postsAscLikesTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = (object)$this->paginator($postsDescLikesTime, 10, null, null, $user);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
        }
    }

    public function feedTrait($users, $request)
    {
        $now = Carbon::now()->format('Y-m-d H:i:s');
        $last_3_months = Carbon::now()->subMonths(3)->format('Y-m-d');
        $last_month = Carbon::now()->subMonth()->format('Y-m-d');
        $last_week = Carbon::now()->subWeek()->format('Y-m-d');
        switch ($request->timeFilter) {
            case 0:
                $data = Post::orderBy('id', 'desc')->whereIn('user_id', $users)->whereNull('schedule')
                    ->orwhere('schedule', '<', $now)->whereIn('user_id', $users)
                    ->paginate(10);
                return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
            case 1:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = Post::orderBy('id', 'asc')->whereIn('user_id', $users)->whereNull('schedule')
                        ->orwhere('schedule', '<', $now)->whereIn('user_id', $users)
                        ->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = Post::orderBy('id', 'desc')->whereIn('user_id', $users)->whereNull('schedule')
                        ->orwhere('schedule', '<', $now)->whereIn('user_id', $users)
                        ->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = Post::orderBy('likes', 'asc')->whereIn('user_id', $users)->whereNull('schedule')
                        ->orwhere('schedule', '<', $now)->whereIn('user_id', $users)
                        ->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = Post::orderBy('likes', 'desc')->whereIn('user_id', $users)->whereNull('schedule')
                        ->orwhere('schedule', '<', $now)->whereIn('user_id', $users)
                        ->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 2:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = Post::orderBy('id', 'asc')->whereIn('user_id', $users)->whereDate('created_at', '>', $last_3_months)->whereNull('schedule')
                        ->orwhere('schedule', '<', $now)->whereIn('user_id', $users)
                        ->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = Post::orderBy('id', 'desc')->whereIn('user_id', $users)->whereDate('created_at', '>', $last_3_months)->whereNull('schedule')
                        ->orwhere('schedule', '<', $now)->whereIn('user_id', $users)
                        ->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = Post::orderBy('likes', 'asc')->whereIn('user_id', $users)->whereDate('created_at', '>', $last_3_months)->whereNull('schedule')
                        ->orwhere('schedule', '<', $now)->whereIn('user_id', $users)
                        ->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = Post::orderBy('likes', 'desc')->whereIn('user_id', $users)->whereDate('created_at', '>', $last_3_months)->whereNull('schedule')
                        ->orwhere('schedule', '<', $now)->whereIn('user_id', $users)
                        ->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 3:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = Post::orderBy('id', 'asc')->whereIn('user_id', $users)->whereDate('created_at', '>', $last_month)->whereNull('schedule')
                        ->orwhere('schedule', '<', $now)->whereIn('user_id', $users)
                        ->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = Post::orderBy('id', 'desc')->whereIn('user_id', $users)->whereDate('created_at', '>', $last_month)->whereNull('schedule')
                        ->orwhere('schedule', '<', $now)->whereIn('user_id', $users)
                        ->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = Post::orderBy('likes', 'asc')->whereIn('user_id', $users)->whereDate('created_at', '>', $last_month)->whereNull('schedule')
                        ->orwhere('schedule', '<', $now)->whereIn('user_id', $users)
                        ->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = Post::orderBy('likes', 'desc')->whereIn('user_id', $users)->whereDate('created_at', '>', $last_month)->whereNull('schedule')
                        ->orwhere('schedule', '<', $now)->whereIn('user_id', $users)
                        ->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 4:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = Post::orderBy('id', 'asc')->whereIn('user_id', $users)->whereDate('created_at', '>', $last_week)->whereNull('schedule')
                        ->orwhere('schedule', '<', $now)->whereIn('user_id', $users)
                        ->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = Post::orderBy('id', 'desc')->whereIn('user_id', $users)->whereDate('created_at', '>', $last_week)->whereNull('schedule')
                        ->orwhere('schedule', '<', $now)->whereIn('user_id', $users)
                        ->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = Post::orderBy('likes', 'asc')->whereIn('user_id', $users)->whereDate('created_at', '>', $last_week)->whereNull('schedule')
                        ->orwhere('schedule', '<', $now)->whereIn('user_id', $users)
                        ->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = Post::orderBy('likes', 'desc')->whereIn('user_id', $users)->whereDate('created_at', '>', $last_week)->whereNull('schedule')
                        ->orwhere('schedule', '<', $now)->whereIn('user_id', $users)
                        ->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
        }
    }

    public function FilterFirst($user, $request)
    {
        $last_3_months = Carbon::now()->subMonths(3)->format('Y-m-d');
        $last_month = Carbon::now()->subMonth()->format('Y-m-d');
        $last_week = Carbon::now()->subWeek()->format('Y-m-d');
        switch ($request->timeFilter) {
            case 0:
                $data = $user->bookmarksAll()->orderBy('posts.id', 'desc')->paginate(10);
                return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
            case 1:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->orderBy('posts.id', 'asc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->orderBy('posts.id', 'desc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->orderBy('posts.likes', 'asc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->orderBy('posts.likes', 'desc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 2:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->orderby('posts.id', 'asc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->orderBy('posts.likes', 'asc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->orderBy('posts.likes', 'desc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 3:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->orderby('posts.id', 'asc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->orderBy('posts.likes', 'asc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->orderBy('posts.likes', 'desc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 4:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->orderby('posts.id', 'asc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->orderBy('posts.likes', 'asc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->orderBy('posts.likes', 'desc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
        }
    }

    public function paginator($items, $perPage, $page = null, $type, $user)
    {
        $options = ['path' => url('api/my-posts' . '/' . $user->id)];
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
