<?php
/**
 * Created by PhpStorm.
 * User: Aspire
 * Date: 22.09.2021
 * Time: 16:10
 */

namespace App\Http\Traits;

use App\Models\Device;

trait NotificationTrait
{
    use PushNotificationTrait;

    public function notification($object, $type, $recipient)
    {
        $object->notifications()->create([
            'sender_id' => auth()->user()->id,
            'type' => $type,
            'user_id' => $recipient
        ]);
    }

    public function push($tokens, $object_id)
    {

        $tokens->each(function ($item) use ($object_id) {
            $this->sendNotification(
                $item->token,
                "Fizl",
                'New like from ' . auth()->user()->username,
                ['post_id' => $object_id]
            );
        });
    }

    public function pushComment($tokens, $object_id)
    {

        $tokens->each(function ($item) use ($object_id) {
            $this->sendNotification(
                $item->token,
                "Fizl",
                'New comment from ' . auth()->user()->username,
                ['post_id' => $object_id]
            );
        });
    }

    public function subscribe($tokens, $object)
    {

        $tokens->each(function ($item) use ($object) {
            $this->sendNotification(
                $item->token,
                "Fizl",
                'You have a new subscriber ' . auth()->user()->username,
                ['user_id' => $object]
            );
        });
    }
}