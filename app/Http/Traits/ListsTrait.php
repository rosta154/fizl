<?php
/**
 * Created by PhpStorm.
 * User: Aspire
 * Date: 10.11.2021
 * Time: 16:37
 */

namespace App\Http\Traits;


trait ListsTrait
{
    public function lists($object, $type, $user, $list)
    {
        $object->customlist()->create([
            'list_id' => $list,
            'type' => $type,
            'user_id' => $user->id
        ]);
    }
}
