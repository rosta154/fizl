<?php
/**
 * Created by PhpStorm.
 * User: Aspire
 * Date: 17.12.2021
 * Time: 12:59
 */

namespace App\Http\Traits;

use App\Models\Message;
use App\Models\User;
use App\Http\Resources\Collection;
use App\Http\Resources\Chat\LastMessageResource;
use App\Models\Room;
use App\Models\RoomUser;
use App\Events\MessageUpdateEvent;

trait ChatTrait
{
    public function sortByDesc($user)
    {
        $chatsSender = Message::orderBy('created_at', 'desc')->where('sender_id', $user->id)->where('recipient_id', '!=', $user->id)->pluck('recipient_id')->toArray();
        $chatsRecipient = Message::orderBy('created_at', 'desc')->where('sender_id', '!=', $user->id)->where('recipient_id', $user->id)->pluck('sender_id')->toArray();
        $allChats = array_merge($chatsSender, $chatsRecipient);
        $userIds = array_values(array_unique($allChats));
        $chatsWithUsers = User::whereIn('id', $userIds)->paginate(10);
        $chatsWithUsers->setCollection(
            $chatsWithUsers->sortBy(function ($user, $key) use ($userIds) {
                return array_search($user->id, $userIds);
            })
        );
        return $this->respondWithPagination($chatsWithUsers, new Collection(LastMessageResource::collection($chatsWithUsers)));
    }

    public function sentMessage($user)
    {
        $chatsRecipientSent = Message::orderBy('created_at', 'desc')
            ->where('sender_id', '!=', $user->id)
            ->where('recipient_id', $user->id)
            ->where('check', Message::SENT)
            ->pluck('sender_id')
            ->toArray();
        $chatsSender = Message::orderBy('created_at', 'desc')
            ->where('sender_id', $user->id)
            ->where('recipient_id', '!=', $user->id)
            ->pluck('recipient_id')
            ->toArray();
        $chatsRecipient = Message::orderBy('created_at', 'desc')
            ->where('sender_id', '!=', $user->id)
            ->where('recipient_id', $user->id)
            ->where('check', Message::READ)
            ->pluck('sender_id')
            ->toArray();
        $allChats = array_merge($chatsRecipientSent, $chatsSender, $chatsRecipient);
        $userIds = array_values(array_unique($allChats));
        $chatsWithUsers = User::whereIn('id', $userIds)->paginate(10);
        $chatsWithUsers->setCollection(
            $chatsWithUsers->sortBy(function ($user, $key) use ($userIds) {
                return array_search($user->id, $userIds);
            })
        );
        return $this->respondWithPagination($chatsWithUsers, new Collection(LastMessageResource::collection($chatsWithUsers)));
    }

    public function readMessage($user)
    {
        $chatsSender = Message::orderBy('created_at', 'desc')->where('sender_id', $user->id)->where('recipient_id', '!=', $user->id)->pluck('recipient_id')->toArray();
        $chatsRecipient = Message::orderBy('created_at', 'desc')->where('sender_id', '!=', $user->id)->where('recipient_id', $user->id)->pluck('sender_id')->toArray();
        $allChats = array_merge($chatsSender, $chatsRecipient);
        $userIds = array_values(array_unique($allChats));
        $chatsWithUsers = User::whereIn('id', $userIds)->paginate(10);
        $chatsWithUsers->setCollection(
            $chatsWithUsers->sortBy(function ($user, $key) use ($userIds) {
                return array_search($user->id, $userIds);
            })
        );
        $messages = Message::orderBy('id', 'desc')->where('sender_id', '!=', $user->id)->where('recipient_id', $user->id)->where('check', Message::SENT)
            ->get();

        $users = Message::orderBy('id', 'desc')->where('sender_id', '!=', $user->id)->where('recipient_id', $user->id)->where('check', Message::SENT)
            ->pluck('sender_id')->toArray();
        $userIdsSender = array_values(array_unique($users));


        $roomsCurrentUser = RoomUser::where('user_id', $user->id)->pluck('room_id');
        foreach ($messages as $message) {
            foreach ($userIdsSender as $value) {
                $sender = User::whereId($value)->first();
                $message->update(['check' => Message::READ]);
                $checkUserRoom = RoomUser::whereIn('room_id', $roomsCurrentUser)->where('user_id', $sender->id)->first();
                $room_id = $checkUserRoom->room_id;
                broadcast(new MessageUpdateEvent($message, $sender, $room_id))->toOthers();
            }
        }
        return $this->respondWithPagination($chatsWithUsers, new Collection(LastMessageResource::collection($chatsWithUsers)));
    }
}