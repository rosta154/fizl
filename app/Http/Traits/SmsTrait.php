<?php
/**
 * Created by PhpStorm.
 * User: Aspire
 * Date: 26.10.2021
 * Time: 15:00
 */

namespace App\Http\Traits;

use App\Http\Resources\User\RegisterResource;
use App\Models\User;
use App\Models\UserDevice;
use App\Models\UserPhone;
use App\Models\Verification;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Resources\User\UserResource;

trait SmsTrait
{
    /**
     * generate code
     * @return int
     */
    private function generateCode()
    {
        $code = mt_rand(1111, 9999);
        return $code;
    }

    /**
     * send sms
     *
     * @param $to
     * @param $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendSms($to, $code)
    {
        return response()->json(['status' => true, 'message' => 'SMS with the code has been sent to you.', 'code' => $code, 'phone' => $to]);
//        return response()->json(['code' => 0, 'message' => $code]);
//        return response()->json(['code' => 0, 'message' => 'Смс с кодом отправлено']);
    }

    /**
     * check code 2 step auth
     *
     * @param $code
     * @param $verificationCode
     * @param $phone
     * @param $verificationPhone
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkCode($code, $verificationCode, $phone, $verificationPhone, $request)
    {
        if ($verificationCode == $code && $verificationPhone == $phone) {

            if (User::wherePhone($phone)->exists()) {
                $token = null;
                $user = User::wherePhone($phone)->first();

                try {
                    if (!$token = JWTAuth::fromUser($user)) {
                        return response()->json(['message' => 'User not found'], 404);
                    }
                } catch (JWTException $e) {
                    return response()->json(['message' => 'Failed login'], 500);
                }
                if (!is_null($request->device_id)) {
                    UserDevice::firstOrCreate(['user_id' => $user->id, 'device_id' => $request->device_id]);
                }
                Verification::where('phone', $phone)->delete();
                return response()->json(['status' => true, 'message' => 'success login', 'data' => new RegisterResource($user), 'token' => $token]);

            }
        }
        return response()->json(['message' => 'Code not right'], 422);

    }

    /**
     * check code 2 step auth
     *
     * @param $code
     * @param $verificationCode
     * @param $phone
     * @param $verificationPhone
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkCodePhone($code, $verificationCode, $phone, $verificationPhone, $user)
    {
        if ($verificationCode == $code && $verificationPhone == $phone) {
            if (UserPhone::wherePhone($phone)->where('user_id', $user->id)->exists()) {
                $token = null;
                try {
                    if (!$token = JWTAuth::fromUser($user)) {
                        return response()->json(['message' => 'User not found'], 404);
                    }
                } catch (JWTException $e) {
                    return response()->json(['message' => 'Failed login'], 500);
                }
                $data = UserPhone::wherePhone($phone)->where('user_id', $user->id)->first();
                $user->update(['phone' => $phone, 'code' => $data->countryCode, 'country' => $data->country]);
                UserPhone::where('phone', $phone)->delete();
                return response()->json(['status' => true, 'message' => 'success login', 'data' => new UserResource($user), 'token' => $token]);
            }
        }
        return response()->json(['message' => 'Code not right'], 422);

    }
}
