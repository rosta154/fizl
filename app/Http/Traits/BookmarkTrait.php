<?php
/**
 * Created by PhpStorm.
 * User: Aspire
 * Date: 12.10.2021
 * Time: 12:47
 */

namespace App\Http\Traits;

use App\Http\Resources\Collection;
use App\Http\Resources\Post\PostResource;
use App\Models\File;
use App\Http\Resources\Post\PostVideoResource;
use App\Http\Resources\Post\PostPhotoResource;
use App\Models\Post;
use Carbon\Carbon;

trait BookmarkTrait
{
    public function allBookmarks($user, $request)
    {
        $last_3_months = Carbon::now()->subMonths(3)->format('Y-m-d');
        $last_month = Carbon::now()->subMonth()->format('Y-m-d');
        $last_week = Carbon::now()->subWeek()->format('Y-m-d');
        switch ($request->timeFilter) {
            case 0:
                $data = $user->bookmarks()->paginate(10);
                return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
            case 1:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->orderBy('posts.id', 'asc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->orderBy('posts.id', 'desc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->orderBy('posts.likes', 'asc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->orderBy('posts.likes', 'desc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

            case 2:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->orderby('posts.id', 'asc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->orderBy('posts.likes', 'asc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->orderBy('posts.likes', 'desc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 3:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->orderby('posts.id', 'asc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->orderBy('posts.likes', 'asc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->orderBy('posts.likes', 'desc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 4:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->orderby('posts.id', 'asc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->orderBy('posts.likes', 'asc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->orderBy('posts.likes', 'desc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
        }

    }

    public function videos($user, $request)
    {
        $last_3_months = Carbon::now()->subMonths(3)->format('Y-m-d');
        $last_month = Carbon::now()->subMonth()->format('Y-m-d');
        $last_week = Carbon::now()->subWeek()->format('Y-m-d');
        switch ($request->timeFilter) {
            case 0:
                $data = $user->bookmarks()->whereType(Post::TYPE_VIDEO)->paginate(10);
                return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
            case 1:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_VIDEO)->orderBy('posts.id', 'asc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_VIDEO)->orderBy('posts.id', 'desc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_VIDEO)->orderBy('posts.likes', 'asc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_VIDEO)->orderBy('posts.likes', 'desc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

            case 2:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_VIDEO)->orderby('posts.id', 'asc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_VIDEO)->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_VIDEO)->orderby('posts.id', 'asc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_VIDEO)->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 3:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_VIDEO)->orderby('posts.id', 'asc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_VIDEO)->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_VIDEO)->orderby('posts.likes', 'asc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_VIDEO)->orderby('posts.likes', 'desc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 4:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_VIDEO)->orderby('posts.id', 'asc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_VIDEO)->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_VIDEO)->orderby('posts.likes', 'asc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_VIDEO)->orderby('posts.likes', 'desc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

        }

    }

    public
    function photos($user, $request)
    {
        $last_3_months = Carbon::now()->subMonths(3)->format('Y-m-d');
        $last_month = Carbon::now()->subMonth()->format('Y-m-d');
        $last_week = Carbon::now()->subWeek()->format('Y-m-d');
        switch ($request->timeFilter) {
            case 0:
                $data = $user->bookmarks()->whereType(Post::TYPE_PHOTO)->paginate(10);
                return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
            case 1:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_PHOTO)->orderBy('posts.id', 'asc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_PHOTO)->orderBy('posts.id', 'desc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_PHOTO)->orderBy('posts.likes', 'asc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_PHOTO)->orderBy('posts.likes', 'desc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

            case 2:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_PHOTO)->orderby('posts.id', 'asc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_PHOTO)->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_PHOTO)->orderby('posts.likes', 'asc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_PHOTO)->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 3:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_PHOTO)->orderby('posts.id', 'asc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_PHOTO)->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_PHOTO)->orderby('posts.likes', 'asc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);

                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_PHOTO)->orderby('posts.likes', 'desc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 4:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_PHOTO)->orderby('posts.id', 'asc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_PHOTO)->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_PHOTO)->orderby('posts.likes', 'asc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_PHOTO)->orderby('posts.likes', 'desc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

        }

    }

    public
    function text($user, $request)
    {
        $last_3_months = Carbon::now()->subMonths(3)->format('Y-m-d');
        $last_month = Carbon::now()->subMonth()->format('Y-m-d');
        $last_week = Carbon::now()->subWeek()->format('Y-m-d');
        switch ($request->timeFilter) {
            case 0:
                $data = $user->bookmarks()->whereType(Post::TYPE_POST)->paginate(10);
                return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
            case 1:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POST)->orderBy('posts.id', 'asc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POST)->orderBy('posts.id', 'desc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POST)->orderBy('posts.likes', 'asc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POST)->orderBy('posts.likes', 'desc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

            case 2:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POST)->orderby('posts.id', 'asc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POST)->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POST)->orderBy('posts.likes', 'asc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POST)->orderby('posts.likes', 'desc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 3:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POST)->orderby('posts.id', 'asc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POST)->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POST)->orderby('posts.likes', 'asc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);

                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POST)->orderby('posts.likes', 'desc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 4:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POST)->orderby('posts.id', 'asc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POST)->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POST)->orderby('posts.likes', 'asc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POST)->orderby('posts.likes', 'desc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

        }
    }

    public
    function poll($user, $request)
    {
        $last_3_months = Carbon::now()->subMonths(3)->format('Y-m-d');
        $last_month = Carbon::now()->subMonth()->format('Y-m-d');
        $last_week = Carbon::now()->subWeek()->format('Y-m-d');
        switch ($request->timeFilter) {
            case 0:
                $data = $user->bookmarks()->whereType(Post::TYPE_POLL)->paginate(10);
                return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
            case 1:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POLL)->orderBy('posts.id', 'asc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POLL)->orderBy('posts.id', 'desc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POLL)->orderBy('posts.likes', 'asc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POLL)->orderBy('posts.likes', 'desc')->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

            case 2:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POLL)->orderby('posts.id', 'asc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POLL)->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POLL)->orderby('posts.likes', 'asc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POLL)->orderby('posts.likes', 'desc')->whereDate('posts.created_at', '>', $last_3_months)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 3:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POLL)->orderby('posts.id', 'asc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POLL)->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POLL)->orderby('posts.likes', 'asc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POLL)->orderby('posts.likes', 'desc')->whereDate('posts.created_at', '>', $last_month)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
            case 4:
                if ($request->sortType == 1 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POLL)->orderby('posts.id', 'asc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 1 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POLL)->orderby('posts.id', 'desc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

                if ($request->sortType == 2 && $request->sortDirection == 1) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POLL)->orderby('posts.likes', 'asc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }
                if ($request->sortType == 2 && $request->sortDirection == 2) {
                    $data = $user->bookmarksAll()->whereType(Post::TYPE_POLL)->orderby('posts.likes', 'desc')->whereDate('posts.created_at', '>', $last_week)->paginate(10);
                    return $this->respondWithPagination($data, new Collection(PostResource::collection($data)));
                }

        }
    }
}