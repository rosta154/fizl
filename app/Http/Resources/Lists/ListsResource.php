<?php

namespace App\Http\Resources\Lists;

use App\Http\Resources\Collection;
use App\Http\Resources\User\UserResource;
use App\Models\Lists;
use App\Models\Network;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class ListsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        if ($this->type == Lists::TYPE_CUSTOM) {
            return [
                'id' => $this->id,
                'title' => $this->title,
                'type' => $this->type,
                'posts' => $this->posts()->count(),
                'users' => $this->users()->count(),
                'media' => $this->media()->count()
            ];
        }
        if ($this->id == Lists::FOLLOWING) {
            $following = auth()->user()->subscriber();
            return [
                'id' => $this->id,
                'title' => $this->title,
                'type' => $this->type,

                'posts' => Post::whereIn('user_id', $following->pluck('subscription_id'))->count(),
                'users' => $following->count(),
                'avatars' => new Collection(UserResource::collection(User::whereIn('id', $following->pluck('subscription_id'))->get()->take(3)))
            ];
        }
        if ($this->id == Lists::RESTRICTED) {
            return [
                'id' => $this->id,
                'title' => $this->title,
                'type' => $this->type,

//                'posts' => Post::whereIn('user_id',$following->pluck('subscription_id'))->count(),
                'users' => auth()->user()->restricted()->count(),
                'avatars' =>  new Collection(UserResource::collection(User::whereIn('id', auth()->user()->restricted()->pluck('user_id'))->get()->take(3)))

            ];
        }
        if ($this->id == Lists::CLOSE_FRIENDS) {
            return [
                'id' => $this->id,
                'title' => $this->title,
                'type' => $this->type,

//                'posts' => Post::whereIn('user_id',$following->pluck('subscription_id'))->count(),
                'users' => auth()->user()->subscriber()->where('close_friend', Network::CLOSE_FRIEND)->count(),
                'avatars' =>  new Collection(UserResource::collection(User::whereIn('id', auth()->user()->subscriber()->where('close_friend', Network::CLOSE_FRIEND)->pluck('subscription_id'))->get()->take(3)))


            ];
        }
        if ($this->id == Lists::BLOCKED) {
            return [
                'id' => $this->id,
                'title' => $this->title,
                'type' => $this->type,

//                'posts' => Post::whereIn('user_id',$following->pluck('subscription_id'))->count(),
                'users' => auth()->user()->blacklist()->count(),
                'avatars' => new Collection(UserResource::collection(User::whereIn('id', auth()->user()->blacklist()->pluck('user_id'))->get()->take(3)))

            ];
        }
        if ($this->id == Lists::BOOKMARKS) {
            return [
                'id' => $this->id,
                'title' => $this->title,
                'type' => $this->type,
                'posts' => auth()->user()->bookmarks()->count(),
//                'users' =>  auth()->user()->blacklist()->count()
            ];
        }
    }
}
