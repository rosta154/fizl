<?php

namespace App\Http\Resources\Lists;

use App\Http\Resources\Collection;
use App\Http\Resources\Post\PostResource;
use App\Http\Resources\Post\VideoPhotoResource;
use App\Http\Resources\User\UserResource;
use App\Models\CustomList;
use App\Models\File;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class CustomListsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        if ($this->type == CustomList::TYPE_MEDIA) {
            $file = File::where('id', $this->customlistable_id)->first();
            return new VideoPhotoResource($file);
        }
        if ($this->type == CustomList::TYPE_USER) {
            $user = User::where('id', $this->customlistable_id)->first();
            return new UserResource($user);
        }
        if ($this->type == CustomList::TYPE_POST) {
            $post = Post::where('id', $this->customlistable_id)->first();
            return new PostResource($post);
        }
    }
}
