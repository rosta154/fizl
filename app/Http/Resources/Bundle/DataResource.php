<?php

namespace App\Http\Resources\Bundle;

use App\Http\Resources\Collection;
use App\Http\Resources\Promotion\PromotionResource;
use App\Http\Resources\User\UserResource;
use App\Models\Bundle;
use App\Models\Promotion;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class DataResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'price' => $this->subscription_price/100,
            'promotions' => new PromotionResource(Promotion::find($this->promotion->id ?? null)),
            'bundles' => new Collection(BundleResource::collection($this->bundles->where('status',Bundle::STATUS_ACTIVE)?? null))
        ];
    }
}
