<?php

namespace App\Http\Resources\Bundle;

use App\Http\Resources\User\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BundleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'price' => $this->price/100,
            'duration' => $this->duration->title,
            'discount' => $this->discount->title,
            'payment_id' => $this->paymentable->id,
            'user' => new UserResource($this->user),
        ];
    }
}
