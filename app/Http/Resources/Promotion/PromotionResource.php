<?php

namespace App\Http\Resources\Promotion;

use Illuminate\Http\Resources\Json\JsonResource;

class PromotionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'price' => $this->price/100,
            'limit_id' => $this->limit->title,
            'trial_duration_id' => $this->trialDuration->title ?? null,
            'discount_id' => $this->discount->title ?? null,
            'expiration_id' => $this->expiration->title,
            'type' => $this->type,
            'payment_id' => $this->paymentable->id ?? null,
        ];
    }
}
