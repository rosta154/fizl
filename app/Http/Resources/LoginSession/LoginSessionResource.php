<?php

namespace App\Http\Resources\LoginSession;

use Illuminate\Http\Resources\Json\JsonResource;

class LoginSessionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'ip' => $this->ip,
          'device' => $this->device,
          'created_at' => $this->created_at
        ];
    }
}
