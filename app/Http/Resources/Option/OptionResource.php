<?php

namespace App\Http\Resources\Option;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;

class OptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $allAnswers = User::whereHas('options', function ($item) {
            /** @var PollOption $item */
            $item->where('post_id', $this->post_id);
        })->count();
        $optionAnswer = $this->users()->count();
        return [
            'id' => $this->id,
            'name' => $this->name,
            'user_answers' => $optionAnswer,
            'my_answer' => User::whereHas('options', function ($item) {
                /** @var PollOption $item */
                $item->where('option_id', $this->id);
                $item->where('user_id', auth()->user()->id);
            })->count(),
            'answer_percent' => round($optionAnswer !== 0 ? $optionAnswer * 100 / $allAnswers : 0, 1)
        ];
    }
}
