<?php

namespace App\Http\Resources\Notification;

use App\Http\Resources\Comment\AnswerResource;
use App\Http\Resources\Comment\CommentResource;
use App\Http\Resources\Post\PostResource;
use App\Http\Resources\User\UserResource;
use App\Models\Comment;
use App\Models\CommentAnswer;
use App\Models\Notification;
use App\Models\Post;
use App\Models\Reaction;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        $like = Reaction::where('id', $this->notificationable_id)->first();

        if ($this->type == Notification::LIKE_COMMENT) {
            $comment = Comment::find($like->reactionable_id);
            return [
                'id' => $this->id,
                'type' => Notification::LIKE_COMMENT,
                'text' => 'New like on your comment',
                'sender' => new UserResource(User::find($this->sender_id)),
                'comment' => new CommentResource(Comment::find($like->reactionable_id)),
                'post' => new PostResource(Post::find($comment->commentable_id)),
                'created_at' => $this->created_at


            ];
        }
        if ($this->type == Notification::LIKE_POST) {
            return [
                'id' => $this->id,
                'type' => Notification::LIKE_POST,
                'text' => 'New like on your post',
                'sender' => new UserResource(User::find($this->sender_id)),
                'post' => new PostResource(Post::find($like->reactionable_id)),
                'created_at' => $this->created_at

            ];
        }

        if ($this->type == Notification::SUBSCRIBE) {
            return [
                'id' => $this->id,
                'type' => Notification::SUBSCRIBE,
                'text' => 'You have a new subscriber',
                'sender' => new UserResource(User::find($this->sender_id)),
                'created_at' => $this->created_at
            ];
        }
        if ($this->type == Notification::COMMENT) {
            $comment = Comment::find($this->notificationable_id);
            return [
                'id' => $this->id,
                'type' => Notification::COMMENT,
                'text' => 'You have a new comment',
                'sender' => new UserResource(User::find($this->sender_id)),
                'comment' => new CommentResource($comment),
                'post' => new PostResource(Post::find($comment->commentable_id)),
                'created_at' => $this->created_at

            ];
        }

        if ($this->type == Notification::ANSWER_COMMENT) {
            $comment = CommentAnswer::find($this->notificationable_id);
            return [
                'id' => $this->id,
                'type' => Notification::ANSWER_COMMENT,
                'text' => 'You have a new answer to your comment',
                'sender' => new UserResource(User::find($this->sender_id)),
                'answer_comment' => new AnswerResource($comment) ?? null,
                'post' => new PostResource(Post::find($comment->comment->commentable_id)),
                'created_at' => $this->created_at


            ];
        }

        if ($this->type == Notification::LIKE_ANSWER) {
            $comment = CommentAnswer::find($like->reactionable_id) ?? null;
//            dd($like->reactionable_id);
            return [
                'id' => $this->id,
                'type' => Notification::LIKE_ANSWER,
                'text' => 'You have a new like',
                'sender' => new UserResource(User::find($this->sender_id)),
                'answer_comment' => new AnswerResource($comment) ?? null,
                'post' => new PostResource(Post::find($comment->comment->commentable_id)),
                'created_at' => $this->created_at


            ];
        }

    }
}
