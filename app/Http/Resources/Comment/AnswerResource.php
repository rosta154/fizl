<?php

namespace App\Http\Resources\Comment;

use App\Models\Reaction;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;
use App\Http\Resources\User\UserResource;
use App\Http\Resources\Collection;
class AnswerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'message' => $this->message,
            'user_id' => $this->user_id,
            'user' => new UserResource(User::find($this->user_id)),
            'recipient' => new UserResource(User::find($this->recipient_id)),
            'likes' => $this->reactions()->where('reaction', Reaction::REACTION_LIKE)->count(),
            'my_like' => $this->reactions()->where('user_id', auth()->user()->id)->count(),
            'created_at' => $this->created_at,
            'users' => new Collection(UserResource::collection($this->users)),


        ];
    }
}
