<?php

namespace App\Http\Resources\Comment;

use App\Http\Resources\User\UserResource;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Reaction;
use App\Http\Resources\Collection;
class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'message' => $this->message,
            'user_id' => $this->user_id,
            'user' => new UserResource(User::find($this->user_id)),
            'likes' => $this->reactions()->where('reaction', Reaction::REACTION_LIKE)->count(),
            'my_like' => $this->reactions()->where('user_id', auth()->user()->id)->count(),
            'answers_count' => $this->answers()->count(),
            'created_at' => $this->created_at,
            'users' => new Collection(UserResource::collection($this->users)),

        ];
    }
}
