<?php

namespace App\Http\Resources\Post;

use App\Http\Resources\Option\OptionResource;
use App\Models\History;
use App\Models\Lists;
use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use App\Http\Resources\Collection;
use Storage;
use App\Models\Reaction;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $lists = Lists::where('user_id', auth()->user()->id)->whereHas('posts', function ($q) {
            $q->where('customlistable_id', $this->id);
        })
            ->orWhereHas('posts', function ($q) {
                $q->where('customlistable_id', $this->id);
                $q->where('user_id', auth()->user()->id);
            })->get();


        $purchased = History::where('salesman_id', $this->user_id)
            ->whereNull('type')
            ->where('status', History::STATUS_APPROVED)
            ->whereDate('end_date', '>', Carbon::now())
            ->where('user_id', auth()->user()->id)
            ->orWhere('salesman_id', $this->user_id)
            ->whereStatus(History::STATUS_APPROVED)
            ->whereNull('end_date')
            ->whereNull('type')
            ->where('user_id', auth()->user()->id)
            ->exists();
        if ($purchased == false) {
            if (auth()->user()->id == $this->user_id) {
                $purchased = true;

            }
            if (!is_null($this->paymentable) && !is_null($this->price) && auth()->user()->id != $this->user_id ) {
                $purchased = History::where('payment_id', $this->paymentable->id)
                    ->where('user_id', auth()->user()->id)
                    ->whereType('App\Models\Post')
                    ->whereStatus(History::STATUS_APPROVED)
                    ->exists();
            }
            if (is_null($this->paymentable)) {
                $purchased = null;
            }
        }

        if ($this->type == Post::TYPE_PHOTO) {
            return [
                'id' => $this->id,
                'description' => $this->description,
                'schedule' => $this->schedule,
                'type' => $this->type,
                'purchased' => $purchased,
                'price' => $this->price ?? null,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
                'payment_id' => $this->paymentable->id ?? null,
                'pin' => $this->pin ?? null,

                'comments_count' => $this->comments()->count(),
                'user' => new UserResource(User::find($this->user_id)),
                'photos' => new Collection(PostPhotoResource::collection($this->photo)),
                'likes' => $this->reactions()->where('reaction', Reaction::REACTION_LIKE)->count(),
                'my_like' => $this->reactions()->where('user_id', auth()->user()->id)->count(),
                'myBookmark' => $this->bookmarks()->where('user_id', auth()->user()->id)->count(),
                'users' => new Collection(UserResource::collection($this->users)),
                'lists' => $lists,

            ];
        }
        if ($this->type == Post::TYPE_VIDEO) {
            return [
                'id' => $this->id,
                'description' => $this->description,
                'schedule' => $this->schedule,
                'type' => $this->type,
                'purchased' => $purchased,
                'price' => $this->price ?? null,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
                'payment_id' => $this->paymentable->id ?? null,
                'pin' => $this->pin ?? null,

                'comments_count' => $this->comments()->count(),
                'user' => new UserResource(User::find($this->user_id)),
                'videos' => new Collection(PostVideoResource::collection($this->video)),
                'likes' => $this->reactions()->where('reaction', Reaction::REACTION_LIKE)->count(),
                'my_like' => $this->reactions()->where('user_id', auth()->user()->id)->count(),
                'myBookmark' => $this->bookmarks()->where('user_id', auth()->user()->id)->count(),
                'users' => new Collection(UserResource::collection($this->users)),
                'lists' => $lists

            ];
        }
        if ($this->type == Post::TYPE_POLL) {
            return [
                'id' => $this->id,
                'description' => $this->description,
                'duration' => $this->duration,
                'type' => $this->type,
                'purchased' => $purchased,
                'price' => $this->price ?? null,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
                'payment_id' => $this->paymentable->id ?? null,
                'pin' => $this->pin ?? null,

                'comments_count' => $this->comments()->count(),
                'user' => new UserResource(User::find($this->user_id)),
                'answer_count' => User::whereHas('options', function ($item) {
                    $item->where('post_id', $this->id);
                })->count(),
                'options' => new Collection(OptionResource::collection($this->options)),
                'likes' => $this->reactions()->where('reaction', Reaction::REACTION_LIKE)->count(),
                'my_like' => $this->reactions()->where('user_id', auth()->user()->id)->count(),
                'myBookmark' => $this->bookmarks()->where('user_id', auth()->user()->id)->count(),
                'users' => new Collection(UserResource::collection($this->users)),
                'lists' => $lists

            ];
        }
        if ($this->type == Post::TYPE_POST) {
            return [
                'id' => $this->id,
                'description' => $this->description,
                'type' => $this->type,
                'schedule' => $this->schedule,
                'purchased' => $purchased,
                'price' => $this->price ?? null,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
                'payment_id' => $this->paymentable->id ?? null,
                'pin' => $this->pin ?? null,

                'comments_count' => $this->comments()->count(),
                'likes' => $this->reactions()->where('reaction', Reaction::REACTION_LIKE)->count(),
                'my_like' => $this->reactions()->where('user_id', auth()->user()->id)->count(),
                'myBookmark' => $this->bookmarks()->where('user_id', auth()->user()->id)->count(),
                'user' => new UserResource(User::find($this->user_id)),
                'users' => new Collection(UserResource::collection($this->users)),
                'lists' => $lists

            ];
        }
    }
}
