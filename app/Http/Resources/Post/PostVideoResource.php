<?php

namespace App\Http\Resources\Post;

use Illuminate\Http\Resources\Json\JsonResource;
use Storage;
class PostVideoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $file = $this;
        $video = $file ? url(Storage::url($file->path . $file->name)) : null;
        return [
            'id' => $this->id,
            'post_id' => $this->fileable_id,
            'video' => $video
        ];
    }
}
