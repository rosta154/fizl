<?php

namespace App\Http\Resources\Post;

use Illuminate\Http\Resources\Json\JsonResource;
use Storage;
class PostPhotoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $filePhoto = $this;
        $photo = $filePhoto ? url(Storage::url($filePhoto->path . $filePhoto->name)) : null;
        return [
            'id' => $this->id,
            'post_id' => $this->fileable_id,
            'photo' => $photo
        ];
    }
}
