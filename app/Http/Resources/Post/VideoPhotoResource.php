<?php

namespace App\Http\Resources\Post;

use App\Models\File;
use App\Models\Post;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
class VideoPhotoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $file = $this;
        if ($this->type == File::FILE_VIDEO) {
            $video = $file ? url(Storage::url($file->path . $file->name)) : null;
            return [
                'id' => $this->id,
                'post_id' => $this->fileable_id,
                'video' => $video
            ];
        }
        if ($this->type == File::FILE_PHOTO)
        {
            $filePhoto = $this;
            $photo = $filePhoto ? url(Storage::url($filePhoto->path . $filePhoto->name)) : null;
            return [
                'id' => $this->id,
                'post_id' => $this->fileable_id,
                'photo' => $photo
            ];
        }
    }
}
