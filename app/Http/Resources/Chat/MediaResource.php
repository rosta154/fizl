<?php

namespace App\Http\Resources\Chat;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\File;
use Storage;
class MediaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $file = $this;
        if ($this->type == File::FILE_VIDEO) {
            $video = $file ? url(Storage::url($file->path . $file->name)) : null;
            return [
                'id' => $this->id,
                'video' => $video
            ];
        }
        if ($this->type == File::FILE_PHOTO)
        {
            $filePhoto = $this;
            $photo = $filePhoto ? url(Storage::url($filePhoto->path . $filePhoto->name)) : null;
            return [
                'id' => $this->id,
                'photo' => $photo
            ];
        }
    }
}
