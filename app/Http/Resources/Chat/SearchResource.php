<?php

namespace App\Http\Resources\Chat;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;
use App\Http\Resources\User\UserResource;

class SearchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
//        $file = $this->photo()->first();
//        $photo = $file ? url(Storage::url($file->path . $file->name)) : null;

        return [
            'id' => $this['message']['id'],
            'message' => $this['message']['message'],
            'i_am_sender' => $this['message']['sender_id'] == auth()->user()->id,
            'i_am_recipient' => $this['message']['recipient_id'] == auth()->user()->id,
//            'photo' => $photo,
            'sender' => new UserResource(User::find($this['message']['sender_id'])),
            'recipient' => new UserResource(User::find($this['message']['recipient_id'])),
            'created_at' => $this['message']['created_at'],
            'check' => $this['message']['check'],
            'current_url' => $this['current_url'],
            'next_url' =>  $this['next_url'],
            'last_url' =>  $this['last_url'],
        ];
    }
}
