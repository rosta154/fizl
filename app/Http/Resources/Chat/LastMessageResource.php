<?php

namespace App\Http\Resources\Chat;

use App\Models\Message;
use App\Models\MuteChat;
use Illuminate\Http\Resources\Json\JsonResource;
use Storage;

class LastMessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $user = auth()->user();
        $filePhoto = $this->photo()->first();
        $photo = $filePhoto ? url(Storage::url($filePhoto->path . $filePhoto->name)) : url(Storage::url('user/avatar.png'));
        $fileBanner = $this->banner()->first();
        $banner = $fileBanner ? url(Storage::url($fileBanner->path . $fileBanner->name)) : url(Storage::url('user/banner.png'));
        $lastmessage = Message::where('sender_id', $user->id)->where('recipient_id', '=', $this->id)->orwhere('sender_id', '=', $this->id)->where('recipient_id', $user->id)->latest()->pluck('message')->first();
        $check = Message::where('sender_id', $user->id)->where('recipient_id', '=', $this->id)->orwhere('sender_id', '=', $this->id)->where('recipient_id', $user->id)->latest()->pluck('check')->first();
        $time = Message::where('sender_id', $user->id)->where('recipient_id', '=', $this->id)->orwhere('sender_id', '=', $this->id)->where('recipient_id', $user->id)->latest()->pluck('created_at')->first();
        $muted = MuteChat::where('sender_id', $user->id)->where('recipient_id',$this->id)->exists();
        return [
            'id' => $this->id,
            'username' => $this->username,
            'online' => $this->online,
            'last_seen' => $this->last_seen,
            'name' => $this->name,
            'photo' => $photo,
            'banner' => $banner,
            'last_message' => $lastmessage,
            'created_at' => $time,
            'check' => $check,
            'muted' =>  $muted,
        ];
    }
}
