<?php

namespace App\Http\Resources\Chat;

use App\Http\Resources\Collection;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;
use App\Http\Resources\User\UserResource;
use Storage;
class ChatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'message' => $this->message,
            'i_am_sender' => $this->sender_id == auth()->user()->id,
            'i_am_recipient' => $this->recipient_id == auth()->user()->id,
            'sender' => new UserResource(User::find($this->sender_id)),
            'recipient' => new UserResource(User::find($this->recipient_id)),
            'created_at' => $this->created_at,
            'check' => $this->check,
            'user_like' => $this->reactions()->count(),
            'photos' => new Collection(MediaResource::collection($this->photos)),
            'videos' => new Collection(MediaResource::collection($this->videos)),
        ];
    }
}
