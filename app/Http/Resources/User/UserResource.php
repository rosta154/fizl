<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Collection;
use App\Http\Resources\Lists\ListsResource;
use App\Models\Blacklist;
use App\Models\History;
use App\Models\Lists;
use App\Models\Network;
use App\Models\Restricted;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Storage;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $filePhoto = $this->photo()->first();
        $photo = $filePhoto ? url(Storage::url($filePhoto->path . $filePhoto->name)) : url(Storage::url('user/avatar.png'));
        $fileAvatar = $this->banner()->first();
        $banner = $fileAvatar ? url(Storage::url($fileAvatar->path . $fileAvatar->name)) : url(Storage::url('user/banner.png'));;
        $fans = Network::where('subscription_id', $this->id)->where('subscriber_id', '!=', $this->id)->count();
        $following = Network::where('subscriber_id', $this->id)->where('subscription_id', '!=', $this->id)->count();
        $check = Network::where('subscriber_id', auth()->user()->id)->where('subscription_id', $this->id)->exists();
        $my_fans = Network::where('subscription_id', auth()->user()->id)->where('subscriber_id', $this->id)->exists();
        $restricted = Restricted::where('initiator_id', auth()->user()->id)->where('user_id', $this->id)->exists();
        $blocked = Blacklist::where('initiator_id', auth()->user()->id)->where('user_id', $this->id)->exists();
        $lists = Lists::where('user_id', auth()->user()->id)->whereHas('users', function ($q) {
            $q->where('customlistable_id', $this->id);
        })
            ->orWhereHas('users', function ($q) {
                $q->where('customlistable_id', $this->id);
                $q->where('user_id', auth()->user()->id);
            })->get();

        $payment = History::where('salesman_id', $this->id)
            ->whereNull('type')
            ->where('status', History::STATUS_APPROVED)
            ->whereDate('end_date', '>', Carbon::now())
            ->where('user_id', auth()->user()->id)
            ->orWhere('salesman_id', $this->id)
            ->whereStatus(History::STATUS_APPROVED)
            ->whereNull('end_date')
            ->whereNull('type')
            ->where('user_id', auth()->user()->id)
            ->exists();
        return [
            'id' => $this->id,
            'username' => $this->username,
            'name' => $this->name,
            'customer' => $this->customer()->exists(),
            'date_of_birth' => $this->date_of_birth ?? null,
            'gender' => $this->gender ?? null,
            'email' => $this->email,
            'phone' => $this->phone,
            'code' => $this->code,
            'country' => $this->country,
            'bio' => $this->bio,
            'website' => $this->website,
            'location' => $this->location,
            'online' => $this->online,
            'last_seen' => $this->last_seen,
            'avatar' => $photo,
            'banner' => $banner,
            'fans' => $fans,
            'following' => $following,
            'two_step_sms' => $this->two_step_sms,
            'two_step_face' => $this->two_step_face,
            'mySubsription' => $check,
            'myFan' => $my_fans,
            'isRestricted' => $restricted,
            'isBlocked' => $blocked,
            'verify' => $this->verify,
            'paid_subscription' => $payment,
            'payment_id' => $this->paymentable->id ?? null,
            'lists' => $lists,
        ];
    }
}
