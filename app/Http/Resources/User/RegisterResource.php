<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;
use Storage;
use App\Models\Network;
class RegisterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */

    public function toArray($request)
    {
        $filePhoto = $this->photo()->first();
        $photo = $filePhoto ? url(Storage::url($filePhoto->path . $filePhoto->name)) : url(Storage::url('user/avatar.png'));
        $fileAvatar = $this->banner()->first();
        $banner = $fileAvatar ? url(Storage::url($fileAvatar->path . $fileAvatar->name)) : url(Storage::url('user/banner.png'));;
        $fans = Network::where('subscription_id', $this->id)->count();
        $following = Network::where('subscriber_id', $this->id)->count();
//        $check = Network::where('subscriber_id', auth()->user()->id)->where('subscription_id', $this->id)->exists();
        return [
            'id' => $this->id,
            'username' => $this->username,
            'name' => $this->name,
            'date_of_birth' => $this->date_of_birth ?? null,
            'gender' => $this->gender ?? null,
            'email' => $this->email,
            'phone' => $this->phone,
            'country' => $this->country,
            'code' => $this->code,
            'bio' => $this->bio,
            'website' => $this->website,
            'location' => $this->location,
            'online' => $this->online,
            'avatar' => $photo,
            'banner' => $banner,
            'fans' => $fans,
            'following' => $following,
            'two_step_sms' => $this->two_step_sms,
            'two_step_face' => $this->two_step_face,
//            'mySubsription' =>  $check
        ];
    }
}
