<?php

namespace App\Http\Requests\Blacklist;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class BlacklistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'user_id' => [
                'required',
                'exists:users,id',
                Rule::unique('blacklists')->where(function ($query) {
                    return $query->where('initiator_id', \auth()->id());
                }),
            ]
        ];

    }
}
