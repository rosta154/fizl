<?php

namespace App\Http\Requests\Network;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class NetworkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'subscription_id' => [
                'required',
                'exists:users,id',
                Rule::unique('networks')->where(function ($query) {
                    return $query->where('subscriber_id', \auth()->id());
                }),
            ]
        ];
    }
}
