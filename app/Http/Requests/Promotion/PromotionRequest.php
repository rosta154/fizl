<?php

namespace App\Http\Requests\Promotion;

use Illuminate\Foundation\Http\FormRequest;

class PromotionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'limit_id' => 'required|exists:limits,id',
            'trial_duration_id' => 'exists:trial_durations,id',
            'discount_id' => 'exists:discounts,id',
            'expiration_id' => 'required|exists:expirations,id',
            'message' => '',
        ];
    }
}
