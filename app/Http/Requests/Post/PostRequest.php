<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => '',
            'duration' => 'required_without_all:photos,videos,description|date|after:now',
            'photos' => 'required_without_all:duration,description,videos',
            'videos' => 'required_without_all:duration,description,photos',
            'options' => 'required_without_all:photos,videos,description|required_with_all:duration',
            'schedule' => 'date|after:now',
            'users' => 'exists:users,id'
        ];
    }
}
