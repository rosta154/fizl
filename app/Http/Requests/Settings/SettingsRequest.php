<?php

namespace App\Http\Requests\Settings;

use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'toast_new_comment' => 'In:0,1',
            'toast_new_like' => 'In:0,1',
            'push' => 'In:0,1',
            'app_new_comment' => 'In:0,1',
            'app_new_like' => 'In:0,1',
            'app_discounts' => 'In:0,1',
            'app_upcoming' => 'In:0,1',
            'email' => 'In:0,1',
            'dark_mode' => 'In:0,1',
            'activity_status' => 'In:0,1',
            'subscription_offers' => 'In:0,1'
        ];
    }
}
