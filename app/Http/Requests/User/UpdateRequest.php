<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => [Rule::unique('users')->ignore(auth()->id())],
            'email' => ['email', Rule::unique('users')->ignore(auth()->id())],
            'name' => 'string',
            'bio' => 'string|nullable',
            'website' => 'string|nullable',
            'location' => 'string|max:255|nullable',
            'phone' => 'numeric',
            'gender_id' => 'exists:genders,id',
            'date_of_birth' => 'date'
        ];
    }
}
